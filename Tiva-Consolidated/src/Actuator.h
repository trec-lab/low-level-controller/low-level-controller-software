/*
 * Actuator.h
 * @author: Nick Tremaroli
 * Contains the layout and functions regarding
 * an actuator
 */

#ifndef ACTUATOR_H_
#define ACTUATOR_H_

#include "MotorController.h"
#include "ForceSensor.h"
#include "QuadratureEncoder.h"

/**
 * Actuator
 * Contains all of the data and structures needed by
 * an actuator on the Tiva
 */
struct Actuator
{
    QuadratureEncoder* motorEncoder;

    // 0 or 1 for where the actuator plugs into the sensor board
    uint8_t actuatorNumber;

    ForceSensor* forceSensor;
    MotorController motorController;
};
typedef struct Actuator Actuator;

// constructs an actuator
Actuator actuatorConstruct(uint8_t actuatorNumber, uint32_t QEIBase, QuadratureEncoderBrand encoderBrand, uint16_t QEISampleRate,
                           int32_t QEICountsPerRotation, uint32_t ADCBase,
                           uint8_t ADCSequenceNumber, char ADCGPIORawPortBaseLetter,
                           uint8_t ADCGPIORawPinNumber, ForceSensorBrand forceSensorBrand, float forceSensorSlope,
                           float forceSensorOffset, float forceSensorUpperLimitNewtons,
                           float forceSensorLowerLimitNewtons, uint32_t motorControllerPWMFrequency);

// send a PWM Signal to the actuator
void SendPWMSignal(Actuator* actuator);

#endif /* ACTUATOR_H_ */
