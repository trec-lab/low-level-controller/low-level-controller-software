/**
 * TivaLowLevel.h
 * @author: Nick Tremaroli
 * Contains all of the low-level features and functions
 * of Pandora
 */

#ifndef TIVALOWLEVEL_H
#define TIVALOWLEVEL_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "HAL/PWM_Driver.h"
#include "HAL/Timer_TIVA.h"
#include "EtherCAT_FrameData.h"

#include "Actuator.h"
#include "Joint.h"
#include "StatusLEDs.h"
#include "EasyCAT.h"
#include "IMUs/MPU9250.h"
#include "FTSensors/ATIForceTorqueTransducer.h"

// the total number of sensors to read from on the Tiva
#define MAX_SENSORS_ON_TIVA                 8
#define MAX_PERIPHERALS_TO_ENABLE_ON_TIVA   10

enum TivaInitializationFrameType
{
    Actuator0InitializationFrame,
    Actuator1InitializationFrame,
    Joint0InitializationFrame,
    Joint1InitializationFrame,
    RGBLEDInitializationFrame,
    IMUInitializationFrame,
    FTSensorInitializationFrame
};
typedef enum TivaInitializationFrameType TivaInitializationFrameType;

/**
 * TivaSettings
 * A structure which holds settings regarding how
 * the microcontroller should operate
 */
struct TivaSettings
{
    bool softwareEStopEnable;
};

typedef struct TivaSettings TivaSettings;

/**
 * Tiva
 * A structure consisting of all of the low level
 * variables needed for ONE Tiva. Each Tiva has 2 joints,
 * 2 actuators, various sensors, and variables to keep track
 * of the processID
 */
typedef struct Tiva Tiva;   // put typedef before for the function pointer array
struct Tiva
{
    // the two joints on pandora
    Joint joint0;
    Joint joint1;

    // the two actuators on pandora
    Actuator actuator0;
    Actuator actuator1;

    // the RGB status LED
    RGBLED rgbLed;

    // the settings for the Tiva
    TivaSettings settings;

    // the signals to and from master
    uint8_t signalToMaster;
    uint8_t signalFromMaster;
    uint8_t prevSignalFromMaster;

    // the IMU
    IMU* imu;

    // the FT sensor
    FTSensor* ftSensor;

    // an easyCAT board for communication
    // to and from the master computer
    EasyCAT easyCAT;

    // for synchronizing frames from the master and the Tiva
    uint8_t prevProcessIdFromMaster;
    uint8_t processIdFromMaster;

    // the number of initialization frame received
    uint8_t numberOfInitFramesReceived;

    // a number code representing why the Tiva stopped
    // normal operation
    uint32_t haltCode;

    // a flag to tell if pandora is initialized
    bool initialized;

    // a flag to tell if the tiva has been initialized once
    // since start up. This is used to tell if the tiva
    // is trying to be re-initialized
    bool initializedOnce;

    // function pointer array of all peripherals to enable
    void(*PeripheralEnableFunctions[MAX_PERIPHERALS_TO_ENABLE_ON_TIVA])(Tiva* tiva);

    // function pointer array of all sensors to read from
    void(*SensorReadFunctions[MAX_SENSORS_ON_TIVA])(Tiva* tiva);

    // function point array of all sensors to free before re-initialization
    void(*SensorFreeFunctions[MAX_SENSORS_ON_TIVA])(Tiva* tiva);

    unsigned int numberOfSensorsEnabled;

    unsigned int numberOfPeripheralsEnabled;

    TivaInitializationFrameType currentTivaInitializationFrame;
};

/**
 * floatByteData
 * A union to help with float to int conversion
 */
union FloatByteData
{
    uint8_t Byte[4];
    uint32_t intData;
    float floatData;
};
typedef union FloatByteData FloatByteData;

/**
 * ByteData
 * A union to help with int conversions
 */
union ByteData
{
    uint8_t Byte[4];
    uint16_t Word[2];
    uint32_t intData;
};
typedef union ByteData ByteData;


/*----------------------Initialization functions---------------------*/

// Construct and init the PandoraLowLevel object
Tiva tivaConstruct();

// Only initializes the Tiva's ethercat capabilities
// so it can read initialization data from the master.
// This function is meant to be called before tivaInit
uint8_t tivaInitEtherCAT(Tiva* tiva);

// store the current initialization frame
void StoreCurrentInitFrame(Tiva* tiva);

// initialize all of the tiva's peripherals needed
// after the initialization frame has been parsed
void tivaInit(Tiva* tiva);

/*----------------------Tiva Peripheral Enable Functions----------------------*/

void tivaEnablePWM(Tiva* tiva);
void tivaEnableRGBLED(Tiva* tiva);

void tivaEnableActuator0QuadratureEncoder(Tiva* tiva);
void tivaEnableActuator0ForceSensor(Tiva* tiva);

void tivaEnableActuator1QuadratureEncoder(Tiva* tiva);
void tivaEnableActuator1ForceSensor(Tiva* tiva);

void tivaEnableJoint0AbsoluteEncoder(Tiva* tiva);
void tivaEnableJoint1AbsoluteEncoder(Tiva* tiva);

void tivaEnableIMU(Tiva* tiva);
void tivaEnableFTSensor(Tiva* tiva);

/*----------------------Tiva Sensor Read Functions----------------------*/

void tivaReadActuator0QuadratureEncoder(Tiva* tiva);
void tivaReadActuator0ForceSensor(Tiva* tiva);

void tivaReadActuator1QuadratureEncoder(Tiva* tiva);
void tivaReadActuator1ForceSensor(Tiva* tiva);

void tivaReadJoint0Angles(Tiva* tiva);
void tivaReadJoint1Angles(Tiva* tiva);

void tivaReadIMUData(Tiva* tiva);

void tivaReadFTSensorData(Tiva* tiva);


/*----------------------Tiva Sensor Free Functions----------------------*/
void tivaFreeActuator0QuadratureEncoder(Tiva* tiva);
void tivaFreeActuator0ForceSensor(Tiva* tiva);

void tivaFreeActuator1QuadratureEncoder(Tiva* tiva);
void tivaFreeActuator1ForceSensor(Tiva* tiva);

void tivaFreeJoint0AbsoluteEncoder(Tiva* tiva);
void tivaFreeJoint1AbsoluteEncoder(Tiva* tiva);

void tivaFreeIMU(Tiva* tiva);

void tivaFreeFTSensor(Tiva* tiva);

/*----------------------Low Level Tiva Functions----------------------*/

// Decode the ethercat frame which was sent form the master
void storeDataFromMaster(Tiva* tiva);

// Process the data which was received from the master
bool processDataFromMaster(Tiva* tiva);

// load data in the ethercat frame to sent to the master
void loadDataForMaster(Tiva* tiva);

#endif /* TIVALOWLEVEL_H */
