/**
 * CurrentSensor.h
 * @author: Nick Tremaroli
 * Contains the layout and functions regarding a current sensor
 */
#ifndef CURRENT_SENSOR_H
#define CURRENT_SENSOR_H

#include "HAL/ADC_TIVA.h"

/**
 * CurrentSensor
 * Contains all of the data needed by a current sensor
 */
struct CurrentSensor
{
    uint32_t ADCBase;
    uint32_t sequenceNumber;
    uint32_t ADCGpioSysCtlPortBase;
    uint32_t ADCGpioPortBase;
    uint32_t ADCGpioPinNumber;
    uint32_t ADCChannel;

    bool enabled;

    float slope;
    float offset;

    uint32_t raw;

    float result;
};
typedef struct CurrentSensor CurrentSensor;

// Constructs a current sensor
CurrentSensor currentSensorConstruct(uint32_t ADCBase, uint8_t ADCSequenceNumber,
                                 char ADCGPIORawPortBaseLetter, uint8_t ADCGPIORawPinNumber,
                                 float slope, float offset);

// enables the current sensor
void enableCurrentSensor(CurrentSensor* currentSensor);

// reads the raw current value from the ADC
void readRawCurrent(CurrentSensor* currentSensor);

// gets the amount of current being drawn
void updateCurrentDraw(CurrentSensor* currentSensor);

#endif
