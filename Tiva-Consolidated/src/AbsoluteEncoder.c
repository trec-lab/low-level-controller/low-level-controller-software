/**
 * AbsoluteEncoder.c
 * @author: Nick Tremaroli
 * Contains all of the low-level code for absolute encoder related functions
 */
#include "AbsoluteEncoder.h"

void AbsoluteEncoderEnable(AbsoluteEncoder* encoder)
{
    encoder->vtable->AbsoluteEncoderEnable(encoder);
}

void AbsoluteEncoderRead(AbsoluteEncoder* encoder)
{
    encoder->vtable->AbsoluteEncoderReadSensor(encoder);
}

void AbsoluteEncoderFree(AbsoluteEncoder* encoder)
{
    encoder->vtable->AbsoluteEncoderFree(encoder);
}

/**
 * readAbsEncoderVelocity
 * reds the velocity of the SSI encoder
 * @param encoder: a pointer to the SSI encoder to read the velocity from
 */
//void readAbsEncoderVelocity(SSIEncoder* encoder)
//{
//    const float alpha = 0.99;
//    if((int32_t) (encoder->raw - encoder->rawPrev) < 60000
//    && (int32_t) (encoder->raw - encoder->rawPrev) > -60000)
//    {
//        encoder->rawVel = (int32_t)(encoder->raw - encoder->rawPrev) * encoder->sampleRate;
//        // Filter Velocity
//        encoder->rawVelF = (int32_t) (alpha * encoder->rawVelPrev + (1 - alpha) * encoder->rawVel);
//
//        encoder->rawVelPrev = encoder->rawVelF;
//    }
//    encoder->rawPrev = encoder->raw;  // Update the previous angle
//}

