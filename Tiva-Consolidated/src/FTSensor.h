/**
 * FTSensor.h
 * @author: Nick Tremaroli
 * Contains the layout and functions regarding an FT Sensor
 */

#ifndef FTSENSOR_H
#define FTSENSOR_H

#include <inttypes.h>
#include <stdbool.h>

enum FTSensorBrand
{
    ATIForceTorqueTransducer_FTSensor
};
typedef enum FTSensorBrand FTSensorBrand;

struct FTSensor;

struct FTSensor_vTable
{
    void (*FTSensorEnable)(struct FTSensor*);
    void (*FTSensorRead)(struct FTSensor*);
    void (*FTSensorFree)(struct FTSensor*);
};
typedef struct FTSensor_vTable FTSensor_vTable;

/**
 * FTSensorBias
 * The values stored after calibrating the FT sensor to
 * account for bias values
 */
struct FTSensorBias
{
    float forceXBias;
    float forceYBias;
    float forceZBias;

    float torqueXBias;
    float torqueYBias;
    float torqueZBias;
};
typedef struct FTSensorBias FTSensorBias;

/**
 * FTSensorLimits
 * The upper and lower limits for each direction
 * of force and torque for the FT sensor
 */
struct FTSensorLimits
{
    float forceXUpperLimitNewtons;
    float forceXLowerLimitNewtons;

    float forceYUpperLimitNewtons;
    float forceYLowerLimitNewtons;

    float forceZUpperLimitNewtons;
    float forceZLowerLimitNewtons;

    float torqueXUpperLimitNewtonMeters;
    float torqueXLowerLimitNewtonMeters;

    float torqueYUpperLimitNewtonMeters;
    float torqueYLowerLimitNewtonMeters;

    float torqueZUpperLimitNewtonMeters;
    float torqueZLowerLimitNewtonMeters;
};
typedef struct FTSensorLimits FTSensorLimits;

/**
 * FTSensor
 * Contains all of the data needed by an FT Sensor on the Tiva
 */
struct FTSensor
{
    // the virtual table which allows polymorphism to happen
    const FTSensor_vTable* vtable;

    uint32_t CommunicationBase;

    float forceX;
    float forceY;
    float forceZ;

    float torqueX;
    float torqueY;
    float torqueZ;

    FTSensorLimits limits;

    bool enabled;
};
typedef struct FTSensor FTSensor;

// constructs an FT sensor
FTSensor ftSensorConstruct(uint32_t canRate, uint32_t canBase, char canPortBaseLetter,
                           FTSensorLimits sensorLimits);

// enables the FT sensor
void FTSensorEnable(FTSensor* ftSensor);

// reads data from the FT Sensor
void FTSensorRead(FTSensor* ftSensor);

// frees the FT Sensor
void FTSensorFree(FTSensor* ftSensor);

#endif
