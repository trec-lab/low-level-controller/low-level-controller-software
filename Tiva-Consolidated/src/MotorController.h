/**
 * MotorController.h
 * @author: Nick Tremaroli
 * Contains the layout and functions regarding a Motor Controller
 */

#ifndef MOTORCONTROLLER_H
#define MOTORCONTROLLER_H

#include "HAL/PWM_Driver.h"

/**
 * MotorController
 * a struct which contains all of the data
 * needed to communicate to a motor controller
 * via a PWM
 */
struct MotorController
{
    float dutyCycle;
    uint16_t pwmFrequency;
    uint8_t direction;
};
typedef struct MotorController MotorController;

// Construct a Motor Controller object
MotorController MotorControllerConstruct(uint16_t pwmFrequency);

//Provide a desired integer PWM Frequency, followed by an integer duty cycle.
void setPulseWidth(uint8_t actuator, MotorController* motorController, uint32_t sysClock);

#endif
