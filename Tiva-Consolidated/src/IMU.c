/**
 * IMU.c
 * @author: Nick Tremaroli
 * Contains all of the low-level code for IMU related functions
 */

#include "IMU.h"

void IMUEnable(IMU* imu)
{
    imu->vtable->IMUEnable(imu);
}

void IMURead(IMU* imu)
{
    imu->vtable->IMURead(imu);
}

void IMUFree(IMU* imu)
{
    imu->vtable->IMUFree(imu);
}
