/*
 * Actuator.c
 * @author: Nick Tremaroli
 * Contains the low-level code for actuator related functions
 */
#include "Actuator.h"
#include "QuadratureEncoders/EncoderMRTypeML.h"
#include "ForceSensors/FutekForceSensor.h"

/**
 * actuatorConstruct
 * Constructs an actuator given the corresponding input parameters
 *
 * @param actuatorNumber: The number corresponding to the actuator on the Tiva,
 * actuator 0 is on the 0th side of the sensor board,
 * actuator 1 is on the 1th side of the sensor board
 * @param QEIBase: The QEI Base of the encoder on the actuator
 * @param QEISampleRate: The sample rate of the QEI encoder
 * @param QEICountsPerRotation: The counts per rotation of the QEU Encoder
 * @param ADCBase: The ADC Base of the force sensor on the actuator
 * @param forceSensorSlope: The slope value of the force sensor
 * @param forceSensorOffset: The offset value of the force sensor
 * @param motorControllerPWMFrequency: The PWM frequency of the motor controller
 *
 * @return: an initialized actuator structure
 */
Actuator actuatorConstruct(uint8_t actuatorNumber, uint32_t QEIBase, QuadratureEncoderBrand encoderBrand, uint16_t QEISampleRate,
                           int32_t QEICountsPerRotation, uint32_t ADCBase,
                           uint8_t ADCSequenceNumber, char ADCGPIORawPortBaseLetter,
                           uint8_t ADCGPIORawPinNumber, ForceSensorBrand forceSensorBrand, float forceSensorSlope,
                           float forceSensorOffset, float forceSensorUpperLimitNewtons,
                           float forceSensorLowerLimitNewtons, uint32_t motorControllerPWMFrequency)
{
    Actuator actuator;

    actuator.actuatorNumber = actuatorNumber;

    if(encoderBrand == EncoderMRTypeML_QuadratureEncoder)
        actuator.motorEncoder = EncoderMRTypeMLConstruct(QEIBase, QEISampleRate, QEICountsPerRotation);

    actuator.motorController = MotorControllerConstruct(motorControllerPWMFrequency);

    if(forceSensorBrand == Futek_ForceSensor)
        actuator.forceSensor = FutekForceSensorConstruct(ADCBase, ADCSequenceNumber, ADCGPIORawPortBaseLetter,
                                                    ADCGPIORawPinNumber, forceSensorSlope,
                                                    forceSensorOffset, forceSensorUpperLimitNewtons,
                                                    forceSensorLowerLimitNewtons);

    return actuator;
}

/**
 * SendPWMSignal
 * Sends the PWM Signal to the actuator
 *
 * @param actuator: a pointer to the actuator which a PWM signal
 * will be applied too
 */
void SendPWMSignal(Actuator* actuator)
{
    setPulseWidth(actuator->actuatorNumber, &actuator->motorController, SysCtlClockGet());
}
