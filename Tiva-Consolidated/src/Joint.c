/*
 * Joint.c
 * @author: Nick Tremaroli
 * Contains all of the low-level code for joint related functions
 */
#include "Joint.h"
/**
 * jointConstruct
 * Constructs a joint given the corresponding input parameters
 *
 * @param SSIBase: The SSI Base for the encoder at the joint
 * @param SSIEncoderBrand: The SSI Encoder Brand of the encoder
 * @param sampleRate: The sample rate of the encoder
 * @param: jointReverseFactor: The jointReverseFactor of the joint
 * @param: rawZero: The raw zero of the joint
 * @param: rawForwardRangeOfMotion: The raw forward range of motion of the joint
 * @param: rawBackwardRangeOfMotion: The raw backward range of motion of the joint
 *
 * @return: an initialized joint structure
 */
Joint jointConstruct(uint32_t CommunicationBase, AbsoluteEncoderBrand encoderBrand, uint16_t sampleRate,
                     int8_t jointReverseFactor, float zeroPositionDegrees,
                     float forwardRangeOfMotionDegrees, float backwardRangeOfMotionDegrees)
{
    Joint joint;

    // Construct and initialize the SSI Encoder

    if(encoderBrand == Gurley_AbsoluteEncoder)
        joint.encoder = GurleyEncoderConstruct(CommunicationBase, sampleRate);
    else if(encoderBrand == Orbis_AbsoluteEncoder)
        joint.encoder = OrbisEncoderConstruct(CommunicationBase, sampleRate);

    joint.actualRaw = 0;
    joint.jointReverseFactor = jointReverseFactor;
    joint.rawZero = (uint16_t)(zeroPositionDegrees * (joint.encoder->rawPi / 180.0));
    joint.rawForwardRangeOfMotion = (uint16_t)(forwardRangeOfMotionDegrees * (joint.encoder->rawPi / 180.0));
    joint.rawBackwardRangeOfMotion = (uint16_t)(backwardRangeOfMotionDegrees * (joint.encoder->rawPi / 180.0));

    return joint;
}

/**
 * updateJointAngles
 * updates the joint angle of the joint
 *
 * @param joint: a pointer to the joint which will have
 * its angle updated
 */
void updateJointAngles(Joint* joint)
{
    // read the raw encoder value from the joint
    AbsoluteEncoderRead(joint->encoder);

    joint->encoder->raw %= joint->encoder->rawPi;
    getRawActualValue(joint);
    joint->angleRads = joint->actualRaw * M_PI / joint->encoder->rawPi;
    joint->angleDegrees = joint->actualRaw * 180.0 / joint->encoder->rawPi;

//    // if the brand is an Orbis encoder account for the rollover
//    if (joint->encoder.encoderBrand == Orbis_Encoder)
//    {
//        joint->encoder.raw %= 8191;
//        if(joint->encoder.raw > 65535)
//            joint->encoder.raw -= 65535;
//    }
//
//    // now get the actual angle of the joint
//    getRawActualValue(joint);
//
//    // convert to radians and degrees depending on the type of encoder
//    if (joint->encoder.encoderBrand == Gurley_Encoder)
//        joint->angleRads = joint->actualRaw * M_PI / 65535;
//    else if (joint->encoder.encoderBrand == Orbis_Encoder)
//    {
//        joint->angleRads = joint->actualRaw * ((2 * M_PI) / 16383);
//        joint->angleDegrees = joint->actualRaw * (360 / 16383);
//    }
}

/**
 * getRawActualValue
 * gets the actual angle value of the joint
 *
 * @param joint: a pointer to the joint structure which will
 * have its actual angle calculated
 */
void getRawActualValue(Joint* joint)
{
    // calculate the raw angle with the reverse factor
    int32_t rawAngleReversed = joint->encoder->raw * joint->jointReverseFactor;

    // calculate the raw zero with the reverse factor
    int32_t rawZeroReversed = joint->rawZero * joint->jointReverseFactor;

    int32_t rawAngleAltered = 0;
    int32_t rawZeroAltered = 0;

    // calculate the raw altered angle
    if(rawAngleReversed < 0)
        rawAngleAltered = rawAngleReversed + joint->encoder->rawPi;
    else
        rawAngleAltered = rawAngleReversed;

    // calculate the raw altered zero
    if(rawZeroReversed < 0)
        rawZeroAltered = rawZeroReversed + joint->encoder->rawPi;
    else
        rawZeroAltered = rawZeroReversed;

    // account for a jump below zero
    if(rawZeroAltered - joint->rawBackwardRangeOfMotion < 0 &&
       rawAngleAltered > rawZeroAltered + joint->rawForwardRangeOfMotion)
        rawAngleAltered -= joint->encoder->rawPi;

    // account for a jump above zero
    if(rawZeroAltered + joint->rawForwardRangeOfMotion > joint->encoder->rawPi &&
       rawAngleAltered < rawZeroAltered - joint->rawBackwardRangeOfMotion)
        rawAngleAltered += joint->encoder->rawPi;

    // calculate the actual raw value as a result of the algorithm
    joint->actualRaw = rawAngleAltered - rawZeroAltered;
}

void freeJoint(Joint* joint)
{
    AbsoluteEncoderFree(joint->encoder);
}
