/**
 * StatusLEDs.c
 * @author: Nick Tremaroli
 * Contains all of the low-level code for status LEDs
 */

#include "StatusLEDs.h"

/**
 * ledConstruct
 *
 * Constructs an LED to use for status purposes
 * @param systemControlPeripheralBase: the peripheral base from which the Tiva
 * controls the LED
 * @param GPIOBase: The GPIO Base of the Pin to use
 * @param GPIOPin: The actual Pin of the LED which will be used to turn it on and off
 * @return: a constructed LED
 */
LED ledConstruct(uint32_t systemControlPeripheralBase, uint32_t GPIOBase, uint32_t GPIOPin)
{
    // create an LED
    LED led;

    // store the system control peripheral base
    led.systemControlPeripheralBase = systemControlPeripheralBase;

    // store the GPIOBase
    led.GPIOBase = GPIOBase;

    // store the GPIO pin to turn the LED on and off
    led.GPIOPin = GPIOPin;

    return led;
}

/**
 * rgbLEDContruct
 *
 * Construct an RGB LED to use for status purposes
 * @param redSystemControlPeripheralBase: the peripheral base for the red LED
 * from which the Tiva controls the LED
 * @param redGPIOBase: the GPIOBase of the Pin to use for the red LED
 * @param redGPIOPin: the actual Pin of the red LED which will be used to turn it on and off
 * @param greenSystemControlPeripheralBase: the peripheral base for the green LED
 * from which the Tiva controls the LED
 * @param greenGPIOBase: the GPIOBase of the Pin to use for the green LED
 * @param greenGPIOPin: the actual Pin of the green LED which will be used to turn it on and off
 * @param blueSystemControlPeripheralBase: the peripheral base for the blue LED
 * from which the Tiva controls the LED
 * @param blueGPIOBase: the GPIOBase of the Pin to use for the blue LED
 * @param blueGPIOPin: the actual Pin of the blue LED which will be used to turn it on and off
 * @return: a constructed RGBLED
 */
RGBLED rgbLEDConstruct(uint32_t redSystemControlPeripheralBase, uint32_t redGPIOBase, uint32_t redGPIOPin,
                       uint32_t greenSystemControlPeripheralBase, uint32_t greenGPIOBase, uint32_t greenGPIOPin,
                       uint32_t blueSystemControlPeripheralBase, uint32_t blueGPIOBase, uint32_t blueGPIOPin)
{
    // create the RGB LED
    RGBLED rgbLed;

    // construct the red led
    rgbLed.red = ledConstruct(redSystemControlPeripheralBase, redGPIOBase, redGPIOPin);

    // construct the green led
    rgbLed.green = ledConstruct(greenSystemControlPeripheralBase, greenGPIOBase, greenGPIOPin);

    // construct the blue led
    rgbLed.blue = ledConstruct(blueSystemControlPeripheralBase, blueGPIOBase, blueGPIOPin);

    return rgbLed;
}

/**
 * enable LED
 *
 * enables an LED for use
 * @param led: the LED to enable
 */
void enableLED(LED* led)
{
    // enable the system control peripheral for the LED
    SysCtlPeripheralEnable(led->systemControlPeripheralBase);

    // set the GPIO Pin to function as an LED pin
    GPIOPinTypeGPIOOutput(led->GPIOBase, led->GPIOPin);

    // set the initial value of the LED to off
    GPIOPinWrite(led->GPIOBase, led->GPIOPin, 0);
}

/**
 * enableRGBLED
 *
 * enables an RGB LED for use
 * @param rgbLed: the RGB LED to enable
 */
void enableRGBLED(RGBLED* rgbLed)
{
    // enable the red led
    enableLED(&rgbLed->red);

    // enable the green led
    enableLED(&rgbLed->green);

    // enable the blue led
    enableLED(&rgbLed->blue);
}

/**
 * ledTurnOn
 *
 * turns the led on
 * @param led: the LED to turn on
 */
void ledTurnOn(LED* led)
{
    GPIOPinWrite(led->GPIOBase, led->GPIOPin, led->GPIOPin);
}

/**
 * ledTurnOff
 *
 * turns the led off
 * @param led: the LED to turn off
 */
void ledTurnOff(LED* led)
{
    GPIOPinWrite(led->GPIOBase, led->GPIOPin, 0);
}

/**
 * notConnectedLEDS
 *
 * Configures the LEDS to act a certain
 * way when the Tiva is not connected to the master.
 * @param rgbLed: a pointer to the RGB LED to change the status color
 */
void notConnectedLEDS(RGBLED* rgbLed)
{
    // initialize the static variables for
    // the blinking ability
    static bool led_on = false;
    static uint32_t led_start = 0;
    static uint32_t led_current = 0;

    // Turn on Red after a certain time
    if (!led_on && (abs(led_current - led_start) > 100))
    {
        ledTurnOn(&rgbLed->red);
        ledTurnOff(&rgbLed->green);
        ledTurnOff(&rgbLed->blue);
        led_on = true;
        led_start = 0;
        led_current = 0;
    }

    // Turn off Red after a certain time
    else if (led_on && (abs(led_current - led_start) > 50))
    {
        ledTurnOff(&rgbLed->red);
        led_on = false;
        led_start = 0;
        led_current = 0;
    }
    led_current++;
}

/**
 * idleLEDS
 *
 * Configures the LEDS to act a certain
 * way when the master is sending an idle
 * signal to the Tiva.
 * @param rgbLed: a pointer to the RGB LED to change the status color
 */
void idleLEDS(RGBLED* rgbLed)
{
    // solid purple
    ledTurnOn(&rgbLed->red);
    ledTurnOff(&rgbLed->green);
    ledTurnOn(&rgbLed->blue);
}

/**
 * haltLEDS
 *
 * configures the LEDS to act a certain
 * way when the master sends a halt signal
 * to the Tiva.
 * @param rgbLed: a pointer to the RGB LED to change the status color
 */
void haltLEDS(RGBLED* rgbLed)
{
    // solid red
    ledTurnOn(&rgbLed->red);
    ledTurnOff(&rgbLed->green);
    ledTurnOff(&rgbLed->blue);
}

/**
 * controlLEDS
 *
 * configures the LEDs to act a certain
 * way when the master is sending a control
 * signal to the Tiva
 * @param rgbLed: a pointer to the RGB LED to change the status color
 */
void controlLEDS(RGBLED* rgbLed)
{
    // solid blue
    ledTurnOff(&rgbLed->red);
    ledTurnOff(&rgbLed->green);
    ledTurnOn(&rgbLed->blue);
}

