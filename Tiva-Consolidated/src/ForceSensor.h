/**
 * ForceSensor.h
 * @author: Nick Tremaroli
 * Contains the layout and functions regarding a force sensor
 */
#ifndef FORCE_SENSOR_H
#define FORCE_SENSOR_H

#include <inttypes.h>
#include <stdbool.h>

enum ForceSensorBrand
{
    Futek_ForceSensor
};
typedef enum ForceSensorBrand ForceSensorBrand;

struct ForceSensor;

struct ForceSensor_vTable
{
    void (*ForceSensorEnable)(struct ForceSensor*);
    void (*ForceSensorReadForce)(struct ForceSensor*);
    void (*ForceSensorReadFree)(struct ForceSensor*);
};
typedef struct ForceSensor_vTable ForceSensor_vTable;

/**
 * ForceSensor
 * Contains all of the data needed by a force sensor
 * on the Tiva
 */
struct ForceSensor
{
    const ForceSensor_vTable* vtable;

    uint32_t CommunicationBase;

    bool enabled;

    float upperLimitNewtons;
    float lowerLimitNewtons;

    uint32_t raw;
    float newtons;
};
typedef struct ForceSensor ForceSensor;

// enables a force sensor
void ForceSensorEnable(ForceSensor* forceSensor);

// read the force
void ForceSensorReadForce(ForceSensor* forceSensor);

void ForceSensorFree(ForceSensor* forceSensor);

#endif /* FORCE_SENSOR_H */
