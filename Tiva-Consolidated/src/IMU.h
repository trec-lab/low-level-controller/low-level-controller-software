/**
 * IMU.h
 * @author: Nick Tremaroli
 * Contains the layout and functions regarding an FT Sensor
 */

#ifndef IMU_IMU_TIVA_H_
#define IMU_IMU_TIVA_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "inc/hw_memmap.h"

#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"

#include "HAL/I2C_Tiva.h"

#define GYROFACTOR_2000DPS      ((2000.0 / 32767.5))
#define ACCELEROMETERFACTOR_16G  (9.81 * (16.0/32767.5))

enum IMUBrand
{
    MPU9250_IMU
};
typedef enum IMUBrand IMUBrand;

struct IMU;

struct IMU_vTable
{
    void (*IMUEnable)(struct IMU*);
    void (*IMURead)(struct IMU*);
    void (*IMUFree)(struct IMU*);
};
typedef struct IMU_vTable IMU_vTable;

/**
 * AccelerationData
 * Holds the raw data and actual data regarding
 * acceleration data
 */
struct AccelerationData
{
    // raw data
    int16_t AxRaw;
    int16_t AyRaw;
    int16_t AzRaw;

    // actual floating point data
    float Ax;
    float Ay;
    float Az;
};
typedef struct AccelerationData AccelerationData;

/**
 * GyroData
 * Holds the raw data, actual data and gyro biases
 * regarding gyro data
 */
struct GyroData
{
    // raw data
    int16_t GxRaw;
    int16_t GyRaw;
    int16_t GzRaw;

    // actual data
    float Gx;
    float Gy;
    float Gz;

};
typedef struct GyroData GyroData;

struct GyroBiasData
{
    // gyro bias data
    float GxBias;
    float GyBias;
    float GzBias;
};
typedef struct GyroBiasData GyroBiasData;

/**
 * MagnetometerData
 * Holds the raw data, actual data, magnetometer biases,
 * factory biases, and scale regarding mangetometer data
 */
struct MagnetometerData
{
    // raw data
    int16_t MxRaw;
    int16_t MyRaw;
    int16_t MzRaw;

    // actual data
    float Mx;
    float My;
    float Mz;
};
typedef struct MagnetometerData MagnetometerData;

struct MagnetometerFactoryBiasData
{
    // factory bias data
    float MxFactoryBias;
    float MyFactoryBias;
    float MzFactoryBias;
};
typedef struct MagnetometerFactoryBiasData MagnetometerFactoryBiasData;

struct MagnetometerBiasData
{
    // bias data
    float MxBias;
    float MyBias;
    float MzBias;
};
typedef struct MagnetometerBiasData MagnetometerBiasData;

struct MagnetometerScalingData
{
    // scale data
    float MxScale;
    float MyScale;
    float MzScale;
};
typedef struct MagnetometerScalingData MagnetometerScalingData;

/**
 * IMU
 * Contains all of the data needed by an IMU on the Tiva
 */
struct IMU
{
    const IMU_vTable* vtable;

    uint32_t CommunicationBase;

    AccelerationData accelerationData;
    GyroData gyroData;

    bool enabled;

};
typedef struct IMU IMU;

void IMUEnable(IMU* imu);

void IMURead(IMU* imu);

void IMUFree(IMU* imu);

#endif /* IMU_IMU_TIVA_H_ */
