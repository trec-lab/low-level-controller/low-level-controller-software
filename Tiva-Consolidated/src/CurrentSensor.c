/**
 * CurrentSensor.c
 * @author: Nick Tremaroli
 * Contains all of the low-level code for current sensor related functions
 */

#include "CurrentSensor.h"

/**
 * currentSensorConstruct
 * Constructs a current sensor given the corresponding input parameters
 *
 * @param ADCBase: The ADC Base of the current sensor
 * @param ADCSequenceNumber: The number of the ADC sequencer to use
 * @param ADCGPIORawPortBaseLetter: The letter of the GPIO Port Base to use
 * @param ADCGPIORawPinNumber: The pin number of the GPIO Pin to use
 * @param slope: The slope of the ADC curve for conversion
 * @param offset: The offset of the ADC curve for conversion
 */
CurrentSensor currentSensorConstruct(uint32_t ADCBase, uint8_t ADCSequenceNumber,
                                 char ADCGPIORawPortBaseLetter, uint8_t ADCGPIORawPinNumber,
                                 float slope, float offset)
{
    CurrentSensor currentSensor;

    currentSensor.ADCBase = ADCBase;
    currentSensor.sequenceNumber = ADCSequenceNumber;

    currentSensor.ADCGpioSysCtlPortBase = SYSCTL_PERIPH_GPIOA + (ADCGPIORawPortBaseLetter - 0x41);

    if(ADCGPIORawPortBaseLetter <= 0x44)
        currentSensor.ADCGpioPortBase = GPIO_PORTA_BASE + ((1 << 12) * (ADCGPIORawPortBaseLetter - 0x41));
    else
        currentSensor.ADCGpioPortBase = GPIO_PORTE_BASE + ((1 << 12) * (ADCGPIORawPortBaseLetter - 0x45));

    currentSensor.ADCGpioPinNumber = GPIO_PIN_0 << ADCGPIORawPinNumber;

    if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 3)
        currentSensor.ADCChannel = ADC_CTL_CH0;
    else if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 2)
        currentSensor.ADCChannel = ADC_CTL_CH1;
    else if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 1)
        currentSensor.ADCChannel = ADC_CTL_CH2;
    else if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 0)
        currentSensor.ADCChannel = ADC_CTL_CH3;
    else if(ADCGPIORawPortBaseLetter == 'D' && ADCGPIORawPinNumber == 3)
        currentSensor.ADCChannel = ADC_CTL_CH4;
    else if(ADCGPIORawPortBaseLetter == 'D' && ADCGPIORawPinNumber == 2)
        currentSensor.ADCChannel = ADC_CTL_CH5;
    else if(ADCGPIORawPortBaseLetter == 'D' && ADCGPIORawPinNumber == 1)
        currentSensor.ADCChannel = ADC_CTL_CH6;
    else if(ADCGPIORawPortBaseLetter == 'D' && ADCGPIORawPinNumber == 0)
        currentSensor.ADCChannel = ADC_CTL_CH7;
    else if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 5)
        currentSensor.ADCChannel = ADC_CTL_CH8;
    else if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 4)
        currentSensor.ADCChannel = ADC_CTL_CH9;
    else if(ADCGPIORawPortBaseLetter == 'B' && ADCGPIORawPinNumber == 4)
        currentSensor.ADCChannel = ADC_CTL_CH10;
    else if(ADCGPIORawPortBaseLetter == 'B' && ADCGPIORawPinNumber == 5)
        currentSensor.ADCChannel = ADC_CTL_CH11;

    currentSensor.enabled = false;

    currentSensor.slope = slope;
    currentSensor.offset = offset;

    currentSensor.raw = 0;
    currentSensor.result = 0.0;

    return currentSensor;
}

/**
 * enableCurrentSensor
 * Enables the current sensor
 *
 * @param currentSensor: a pointer to the current sensor to enable
 */
void enableCurrentSensor(CurrentSensor* currentSensor)
{
    if(currentSensor->ADCBase == ADC0_BASE)
    {
        ADCConfig0(currentSensor->sequenceNumber, currentSensor->ADCGpioSysCtlPortBase,
                   currentSensor->ADCGpioPortBase, currentSensor->ADCGpioPinNumber,
                   currentSensor->ADCChannel);
    }
    else if(currentSensor->ADCBase == ADC1_BASE)
    {
        ADCConfig1(currentSensor->sequenceNumber, currentSensor->ADCGpioSysCtlPortBase,
                   currentSensor->ADCGpioPortBase, currentSensor->ADCGpioPinNumber,
                   currentSensor->ADCChannel);
    }
    else
        return;

    currentSensor->enabled = true;
}

/**
 * readRawCurrent
 * reads the raw value from the current sensor
 *
 * @param currentSensor: a pointer to the current sensor to read
 */
void readRawCurrent(CurrentSensor* currentSensor)
{
    // Causes a processor trigger for a sample sequence, trigger the sample sequence.
    ADCProcessorTrigger(currentSensor->ADCBase, currentSensor->sequenceNumber);

    // Wait until the sample sequence has completed.
    // was ADCIntStatusEx
    while(!ADCIntStatus(currentSensor->ADCBase, currentSensor->sequenceNumber, false));

    // Clear the ADC interrupt flag generated upon ADC completion.
    ADCIntClear(currentSensor->ADCBase, currentSensor->sequenceNumber);

    // Obtain single software averaged ADC value.
    ADCSoftwareOversampleDataGet(currentSensor->ADCBase, currentSensor->sequenceNumber, &currentSensor->raw, 1);
}

/**
 * UpdateCurrentDrawn
 * Updates the amount of current being drawn
 *
 * @param currentSensor: a pointer to the current sensor which
 * will update its value
 */
void updateCurrentDraw(CurrentSensor* currentSensor)
{
    readRawCurrent(currentSensor);

    currentSensor->result = currentSensor->raw * currentSensor->slope + currentSensor->offset;
}


