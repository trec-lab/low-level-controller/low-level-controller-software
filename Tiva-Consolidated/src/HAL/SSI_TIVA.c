/**
 * SSI_TIVA.c
 * Contains all of the low-level code for SSI communication
 * on the Tiva
 */
#include "SSI_TIVA.h"


/**
 * SSI0_Gurley_Config
 * Configures a Gurley encoder to work on SSI0
 */
void SSI0_Gurley_Config()
{
    // The SSI0 peripheral must be enabled for use.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);

    // Enable GPIO Port A as SSI0 uses PA2, PA3, PA4, and PA5
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Pin A2 is the SSI0 Clock
    GPIOPinConfigure(GPIO_PA2_SSI0CLK);

    // Pin A3 is the SSI0 slave select
    GPIOPinConfigure(GPIO_PA3_SSI0FSS);

    // Pin PA4 is the SSI0 receive pin
    GPIOPinConfigure(GPIO_PA4_SSI0RX);

    // Pin A5 is the SSI0 transmit pin
    GPIOPinConfigure(GPIO_PA5_SSI0TX);

    // Configure these pins to be used for SSI
    GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_5 | GPIO_PIN_4 | GPIO_PIN_3 | GPIO_PIN_2);

    // Configure SSI0 with the following settings
    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_TI,
                       SSI_MODE_MASTER, 500000, 16);    //16 bit

    // Enable SSI0
    SSIEnable(SSI0_BASE);
}

/**
 * SSI0_Orbis_Config
 * Configures a Orbis encoder to work on SSI0
 */
void SSI0_Orbis_Config()
{
    // The SSI0 peripheral must be enabled for use.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);

    // Enable GPIO Port A as SSI0 uses PA2, PA3, PA4, and PA5
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Pin A2 is the SSI0 Clock
    GPIOPinConfigure(GPIO_PA2_SSI0CLK);

    // Pin A3 is the SSI0 slave select
    GPIOPinConfigure(GPIO_PA3_SSI0FSS);

    // Pin PA4 is the SSI0 receive pin
    GPIOPinConfigure(GPIO_PA4_SSI0RX);

    // Pin A5 is the SSI0 transmit pin
    GPIOPinConfigure(GPIO_PA5_SSI0TX);

    // Configure these pins to be used for SSI
    GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_5 | GPIO_PIN_4 | GPIO_PIN_3 | GPIO_PIN_2);

    // Configure the SSI0 clock with the following settings
    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0,
                       SSI_MODE_MASTER, 500000, 16);    //16 bit

    // Enable SSI0
    SSIEnable(SSI0_BASE);
}

/**
 * SSI0_EasyCAT_Config
 * Configures the EasyCAT to work on SSI0
 */
void SSI0_EasyCAT_Config()
{
    // The SSI0 peripheral must be enabled for use.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);

    // Enable GPIO Port A as SSI0 uses PA2, PA3, PA4, and PA5
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Pin PA2 is the SSI0 Clock
    GPIOPinConfigure(GPIO_PA2_SSI0CLK);

    // Pin PA4 is the SSI0 receive pin
    GPIOPinConfigure(GPIO_PA4_SSI0RX);

    // Pin A5 is the SSI0 transmit pin
    GPIOPinConfigure(GPIO_PA5_SSI0TX);

    // Configure these pins to be used for SSI
    GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_5 | GPIO_PIN_4 | GPIO_PIN_2);

    // Initialize chip select pin separately
    // Using port A pin 3 for chip select
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_3);
    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3, GPIO_PIN_3);

    // Configure SSI clock with the following settings
    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0,
                           SSI_MODE_MASTER, 20000000, 8);

    // Enable SSI0
    SSIEnable(SSI0_BASE);

}

/**
 * SSI1_Gurley_Config
 * Configures a Gurley encoder to work on SSI1
 */
void SSI1_Gurley_Config()
{
    // The SSI1 peripheral must be enabled for use.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);

    // Enable GPIO Port F as SSI1 uses PF0, PF1, PF2, and PF3
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Edit the hardware registry to enable these pins
    HWREG(GPIO_PORTF_BASE+GPIO_O_LOCK) = GPIO_LOCK_KEY;
    HWREG(GPIO_PORTF_BASE+GPIO_O_CR) |= 0X01;
    HWREG(GPIO_PORTF_BASE+GPIO_O_LOCK) = 0;

    // Pin F2 is the SSI1 clock
    GPIOPinConfigure(GPIO_PF2_SSI1CLK);

    // Pin F3 is the SSI1 slave select
    GPIOPinConfigure(GPIO_PF3_SSI1FSS);

    // Pin F0 is the SSI1 receive pin
    GPIOPinConfigure(GPIO_PF0_SSI1RX);

    // Pin F1 is the SSI1 transmit pin
    GPIOPinConfigure(GPIO_PF1_SSI1TX);

    // Configure these pins to be used for SSI1
    GPIOPinTypeSSI(GPIO_PORTF_BASE, GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_0 | GPIO_PIN_1);

    // Configure the SSI1 clock with the following settings
    SSIConfigSetExpClk(SSI1_BASE, SysCtlClockGet(), SSI_FRF_TI,
                       SSI_MODE_MASTER, 500000, 16);//16 bit

    // Enable SSI1
    SSIEnable(SSI1_BASE);
}

/**
 * SSI1_Orbis_Config
 * Configures a Orbis encoder to work on SSI1
 */
void SSI1_Orbis_Config()
{
    // The SSI1 peripheral must be enabled for use.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);

    // Enable GPIO Port F as SSI1 uses PF0, PF1, PF2, and PF3
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Edit the hardware registry to enable these pins
    HWREG(GPIO_PORTF_BASE+GPIO_O_LOCK) = GPIO_LOCK_KEY;
    HWREG(GPIO_PORTF_BASE+GPIO_O_CR) |= 0X01;
    HWREG(GPIO_PORTF_BASE+GPIO_O_LOCK) = 0;

    // Pin F2 is the SSI1 clock
    GPIOPinConfigure(GPIO_PF2_SSI1CLK);

    // Pin F3 is the SSI1 slave select
    GPIOPinConfigure(GPIO_PF3_SSI1FSS);

    // Pin F0 is the SSI1 receive pin
    GPIOPinConfigure(GPIO_PF0_SSI1RX);

    // Pin F1 is the SSI1 transmit pin
    GPIOPinConfigure(GPIO_PF1_SSI1TX);

    // Configure these pins to be used for SSI1
    GPIOPinTypeSSI(GPIO_PORTF_BASE, GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_0 | GPIO_PIN_1);

    // Configure the SSI1 clock with the following settings
    SSIConfigSetExpClk(SSI1_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0,
                       SSI_MODE_MASTER, 500000, 16);//16 bit

    // Enable SSI1
    SSIEnable(SSI1_BASE);
}

/**
 * SSI1_EasyCAT_Config
 * Configures the EasyCAT to work on SSI1
 */
void SSI1_EasyCAT_Config()
{
    // The SSI1 peripheral must be enabled for use.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);

    // Enable GPIO Port F as SSI1 uses PF0, PF1, PF2, and PF3
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Edit the hardware registry to enable these pins
    HWREG(GPIO_PORTF_BASE+GPIO_O_LOCK) = GPIO_LOCK_KEY;
    HWREG(GPIO_PORTF_BASE+GPIO_O_CR) |= 0X01;
    HWREG(GPIO_PORTF_BASE+GPIO_O_LOCK) = 0;

    // Pin F2 is the SSI1 clock
    GPIOPinConfigure(GPIO_PF2_SSI1CLK);

    // Pin F0 is the SSI1 receive pin
    GPIOPinConfigure(GPIO_PF0_SSI1RX);

    // Pin F1 is the SSI1 transmit pin
    GPIOPinConfigure(GPIO_PF1_SSI1TX);

    // Configure these pins to be used for SSI1
    GPIOPinTypeSSI(GPIO_PORTF_BASE, GPIO_PIN_2 | GPIO_PIN_1 | GPIO_PIN_0);

    // Initialize chip select pin separately
    // Using port F pin 3 for chip select
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_3);
    GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, GPIO_PIN_3);

    // Configure SSI clock with the following settings
    SSIConfigSetExpClk(SSI1_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0,
                           SSI_MODE_MASTER, 20000000, 8);

    // Enable SSI1
    SSIEnable(SSI1_BASE);
}

/**
 * SSI2_Gurley_Config
 * Configures a Gurley encoder to work on SSI2
 */
void SSI2_Gurley_Config()
{
    // The SSI2 peripheral must be enabled for use
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI2);

    // Enable Port B as SSI2 uses PB4, PB5, PB6, and PB7
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    // Pin B4 is the SSI2 clock
    GPIOPinConfigure(GPIO_PB4_SSI2CLK);

    // Pin B5 is the SSI2 slave select
    GPIOPinConfigure(GPIO_PB5_SSI2FSS);

    // Pin B6 is the SSI2 receive pin
    GPIOPinConfigure(GPIO_PB6_SSI2RX);

    // Pin B7 is the SSI2 transmit pin
    GPIOPinConfigure(GPIO_PB7_SSI2TX);

    // Confiugre the pins to be used for SSI2
    GPIOPinTypeSSI(GPIO_PORTB_BASE, GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7);


    // Configure SSI2 clock with the following settings
    SSIConfigSetExpClk(SSI2_BASE, SysCtlClockGet(), SSI_FRF_TI,
                           SSI_MODE_MASTER, 500000, 16);

    // enable SSI2
    SSIEnable(SSI2_BASE);
}

/**
 * SSI2_Orbis_Config
 * Configures a Orbis encoder to work on SSI2
 */
void SSI2_Orbis_Config()
{
    // The SSI2 peripheral must be enabled for use
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI2);

    // Enable Port B as SSI2 uses PB4, PB5, PB6, and PB7
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    // Pin B4 is the SSI2 clock
    GPIOPinConfigure(GPIO_PB4_SSI2CLK);

    // Pin B5 is the SSI2 slave select
    GPIOPinConfigure(GPIO_PB5_SSI2FSS);

    // Pin B6 is the SSI2 receive pin
    GPIOPinConfigure(GPIO_PB6_SSI2RX);

    // Pin B7 is the SSI2 transmit pin
    GPIOPinConfigure(GPIO_PB7_SSI2TX);

    // Confiugre the pins to be used for SSI2
    GPIOPinTypeSSI(GPIO_PORTB_BASE, GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7);


    // Configure SSI2 clock with the following settings
    SSIConfigSetExpClk(SSI2_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0,
                           SSI_MODE_MASTER, 500000, 16);

    // enable SSI2
    SSIEnable(SSI2_BASE);
}

/**
 * SSI2_EasyCAT_Config
 * Configures the EasyCAT to work on SSI2
 */
void SSI2_EasyCAT_Config()
{
    // The SSI2 peripheral must be enabled for use
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI2);

    // Enable Port B as SSI2 uses PB4, PB5, PB6, and PB7
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    // Pin B4 is the SSI2 clock
    GPIOPinConfigure(GPIO_PB4_SSI2CLK);

    // Pin B6 is the SSI2 receive pin
    GPIOPinConfigure(GPIO_PB6_SSI2RX);

    // Pin B7 is the SSI2 transmit pin
    GPIOPinConfigure(GPIO_PB7_SSI2TX);

    // Confiugre the pins to be used for SSI2
    GPIOPinTypeSSI(GPIO_PORTB_BASE, GPIO_PIN_4|GPIO_PIN_6|GPIO_PIN_7);

    // Initialize chip select pin separately
    // Using port B pin 5 for chip select
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_5);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, GPIO_PIN_5);

    // Configure SSI clock with the following settings
    SSIConfigSetExpClk(SSI2_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0,
                           SSI_MODE_MASTER, 20000000, 8);

    // enable SSI2
    SSIEnable(SSI2_BASE);
}

/**
 * SSI3_Gurley_Config
 * Configures a Gurley encoder to work on SSI3
 */
void SSI3_Gurley_Config()
{
    // The SSI3 peripheral must be enabled for use
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI3);

    // Enable Port D as SSI3 uses PD0, PD1, PD2, and PD3
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);

    // Pin D0 is the SSI3 clock
    GPIOPinConfigure(GPIO_PD0_SSI3CLK);

    // Pin D1 is the SSI3 slave select
    GPIOPinConfigure(GPIO_PD1_SSI3FSS);

    // Pin D2 is the SSI3 receive pin
    GPIOPinConfigure(GPIO_PD2_SSI3RX);

    // Pin D3 is the SSI3 transmit pin
    GPIOPinConfigure(GPIO_PD3_SSI3TX);

    // Configure the pins to be used for SSI3
    GPIOPinTypeSSI(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);

    // Configure SSI3 clock with the following settings
    SSIConfigSetExpClk(SSI3_BASE, SysCtlClockGet(), SSI_FRF_TI,
                           SSI_MODE_MASTER, 500000, 16);

    // enable SSI3
    SSIEnable(SSI3_BASE);
}

/**
 * SSI3_Orbis_Config
 * Configures a Orbis encoder to work on SSI3
 */
void SSI3_Orbis_Config()
{
    // The SSI3 peripheral must be enabled for use
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI3);

    // Enable Port D as SSI3 uses PD0, PD1, PD2, and PD3
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);

    // Pin B4 is the SSI3 clock
    GPIOPinConfigure(GPIO_PD0_SSI3CLK);

    // Pin B5 is the SSI3 slave select
    GPIOPinConfigure(GPIO_PD1_SSI3FSS);

    // Pin B6 is the SSI3 receive pin
    GPIOPinConfigure(GPIO_PD2_SSI3RX);

    // Pin B7 is the SSI3 transmit pin
    GPIOPinConfigure(GPIO_PD3_SSI3TX);

    // Configure the pins to be used for SSI3
    GPIOPinTypeSSI(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);


    // Configure SSI3 clock with the following settings
    SSIConfigSetExpClk(SSI3_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0,
                           SSI_MODE_MASTER, 500000, 16);

    // enable SSI3
    SSIEnable(SSI3_BASE);
}

void SSI3_EasyCAT_Config()
{
    // The SSI3 peripheral must be enabled for use
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI3);

    // Enable Port D as SSI3 uses PD0, PD1, PD2, and PD3
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);

    // Pin B4 is the SSI3 clock
    GPIOPinConfigure(GPIO_PD0_SSI3CLK);

    // Pin B6 is the SSI3 receive pin
    GPIOPinConfigure(GPIO_PD2_SSI3RX);

    // Pin B7 is the SSI3 transmit pin
    GPIOPinConfigure(GPIO_PD3_SSI3TX);

    // Configure the pins to be used for SSI3
    GPIOPinTypeSSI(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_2|GPIO_PIN_3);

    // Initialize chip select pin separately
    // Using port D pin 1
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_1);
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_1, GPIO_PIN_1);

    // Configure SSI clock with the following settings
    SSIConfigSetExpClk(SSI3_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0,
                           SSI_MODE_MASTER, 20000000, 8);


    // enable SSI3
    SSIEnable(SSI3_BASE);
}

/**
 * SSI3_Config_EasyCAT_Pandora
 * A special configuration for the EasyCAT
 * which is unique to pandora
 */
void SSI3_EasyCAT_Config_Pandora()
{
    // Configure SSI3 pins
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI3);

    // Enable GPIO Port D as SSI3 used PD0, PD2, and PFD3
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    GPIOPinConfigure(GPIO_PD0_SSI3CLK);
    GPIOPinConfigure(GPIO_PD2_SSI3RX);
    GPIOPinConfigure(GPIO_PD3_SSI3TX);

    // Initialize SSI3 pins
    GPIOPinTypeSSI(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_2|GPIO_PIN_3);

    // Initialize chip select pin separately
    // Using port B pin 3 for chip select since PWM_Driver uses port D pin 1.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_3);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_3, GPIO_PIN_3);

    // Configure SSI clock with the following settings
    SSIConfigSetExpClk(SSI3_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0,
                           SSI_MODE_MASTER, 20000000, 8);

    // Enable the SSI3 Base
    SSIEnable(SSI3_BASE);
}
