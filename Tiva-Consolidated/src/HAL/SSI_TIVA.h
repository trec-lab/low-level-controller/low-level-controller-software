/**
 * SSI_TIVA.h
 * @author: Nick Tremaroli
 * Contains the layout and functions regarding
 * SSI related communication of the Tiva
 */

#ifndef SSI_TIVA_H_
#define SSI_TIVA_H_


#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/ssi.h"

// Enable SSI0 for a Gurley Encoder
void SSI0_Gurley_Config(void);

// Enable SSI0 for an Orbis Encoder
void SSI0_Orbis_Config(void);

// Enable SSI0 for the EasyCAT
void SSI0_EasyCAT_Config();

// Enable SSI1 for a Gurley Encoder
void SSI1_Gurley_Config(void);

// Enable SSI1 for an Orbis Encoder
void SSI1_Orbis_Config(void);

// Enable SSI1 for the EasyCAT
void SSI1_EasyCAT_Config();

// Enable SSI2 for the Gurley Encoder
void SSI2_Gurley_Config(void);

// Enable SSI2 for the ORbis Encoder
void SSI2_Orbis_Config(void);

// Enable SSI2 for the EasyCAT
void SSI2_EasyCAT_Config();

// Enable SSI3 for the Gurley Encoder
void SSI3_Gurley_Config(void);

// Enable SSI3 for the ORbis Encoder
void SSI3_Orbis_Config(void);

// Enable SSI2 for the EasyCAT
void SSI3_EasyCAT_Config();

// Enable SSI3 for the EasyCAT on Pandora
// this is a special initialization method used
// just on pandora in which a different slave
// select pin is used than expected
void SSI3_EasyCAT_Config_Pandora(void);

#endif /* SSI_TIVA_H_ */
