/**
 * PWM_Driver.c
 * Contains all of the low-level code for a Tiva PWM
 */
#include "PWM_Driver.h"

/**
 * PWMConfig
 * Configures the PWMs which the Tiva is currently using on Pandora
 */
void PWMConfig()
{
    // Enable GPIO Port E for actuator 0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    // Enable Pin E4 for actuator 0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    GPIOPinConfigure(GPIO_PE4_M0PWM4);
    GPIOPinTypePWM(GPIO_PORTE_BASE, GPIO_PIN_4);

    // Enable GPIO Port B for actuator 1
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    // Use Hardware Reg to unlock and change function of GPIO pin, then lock them again
    HWREG(GPIO_PORTB_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
    HWREG(GPIO_PORTB_BASE + GPIO_O_CR) |= 0x01;
    HWREG(GPIO_PORTB_BASE + GPIO_O_LOCK) = 0;

    // Enable Pin B6 for actuator 1
    GPIOPinConfigure(GPIO_PB6_M0PWM0);
    GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_6);

    // Deadband set to disable delay
    PWMDeadBandDisable(PWM0_BASE, PWM_GEN_0);

    // Configure the actuator 0 direction pin
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_2);

    // Configure the actuator 1 direction pin
    GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_1);

}

