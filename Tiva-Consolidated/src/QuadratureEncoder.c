/**
 * QuadratureEncoder.c
 * @author: Nick Tremaroli
 * Contains all of the low-level code for Quadrature Encoder related functions
 */
#include "QuadratureEncoder.h"

void QuadratureEncoderEnable(QuadratureEncoder* encoder)
{
    encoder->vtable->QuadratureEncoderEnable(encoder);
}

void QuadratureEncoderReadPosition(QuadratureEncoder* encoder)
{
    encoder->vtable->QuadratureEncoderReadPosition(encoder);
}

void QuadratureEncoderReadVelocity(QuadratureEncoder* encoder)
{
    encoder->vtable->QuadratureEncoderReadVelocity(encoder);
}

void QuadratureEncoderFree(QuadratureEncoder* encoder)
{
    encoder->vtable->QuadratureEncoderFree(encoder);
}
