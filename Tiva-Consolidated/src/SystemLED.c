/**
 * SystemLED.c
 * @author: Nick Tremaroli
 * Contains all of the low-level code for System LED
 * related features
 */

#include "SystemLED.h"

/**
 * StatusEasyCATResetTimout
 * The blink code for when a reset timeout issue
 * occurred on the EasyCAY
 *
 * @param statusLED: The status LED to blink the error code
 */
void StatusEasyCATResetTimeout(LED* statusLED)
{
    ledTurnOn(statusLED);
    SysCtlDelay(SMALLDELAY);
    ledTurnOff(statusLED);
    SysCtlDelay(LARGEDELAY);
}

/**
 * StatusEasyCATByteOrderFailed
 * The blink code for when the byte order failed
 * on the easyCAY
 *
 * @param statusLED: The status LED to blink the error code
 */
void StatusEasyCATByteOrderFailed(LED* statusLED)
{
    int i = 0;
    for(;;)
    {
        ledTurnOn(statusLED);
        SysCtlDelay(SMALLDELAY);
        ledTurnOff(statusLED);
        if(i == 1)
            break;
        SysCtlDelay(SMALLDELAY);
        i++;
    }
    SysCtlDelay(LARGEDELAY);
}

/**
 * StatusEasyCATReadyFlagTimoue
 * The blink code for when a ready flag timeout
 * occurred on the EasyCAT
 *
 * @param statusLED: The status LED to blink the error code
 */
void StatusEasyCATReadyFlagTimeout(LED* statusLED)
{
    int i = 0;
    for(;;)
    {
        ledTurnOn(statusLED);
        SysCtlDelay(SMALLDELAY);
        ledTurnOff(statusLED);
        if(i == 2)
            break;
        SysCtlDelay(SMALLDELAY);
        i++;
    }
    SysCtlDelay(LARGEDELAY);

}

/**
 * StatusEasyCATUnkown
 * The blink code for when an unknown error
 * occurred on the EasyCAT
 *
 * @param statusLED: The status LED to blink the error code
 */
void StatusEasyCATUnknown(LED* statusLED)
{
    int i = 0;
    for(;;)
    {
        ledTurnOn(statusLED);
        SysCtlDelay(SMALLDELAY);
        ledTurnOff(statusLED);
        if(i == 3)
            break;
        SysCtlDelay(SMALLDELAY);
        i++;
    }
    SysCtlDelay(LARGEDELAY);
}
