/**
 * EncoderMRTypeML
 * @author: Nick Tremaroli
 * Contains the low-level code for functions related
 * to the Encoder MR Type ML Quadrature Encoder
 */

#ifndef ENCODER_MR_TYPE_ML_H
#define ENCODER_MR_TYPE_ML_H

#include "../QuadratureEncoder.h"

struct EncoderMRTypeML
{
    QuadratureEncoder ENCODER_BASE;

    // add any fields specific to the Encoder MR Type ML below
    // ...
};
typedef struct EncoderMRTypeML EncoderMRTypeML;

QuadratureEncoder* EncoderMRTypeMLConstruct(uint32_t QEIBase, uint16_t sampleRate, int32_t countsPerRotation);

void EncoderMRTypeMLEnable(QuadratureEncoder* encoderBase);

void EncoderMRTypeMLReadPosition(QuadratureEncoder* encoderBase);

void EncoderMRTypeMLReadVelocity(QuadratureEncoder* encoderBase);

void EncoderMRTypeMLFree(QuadratureEncoder* encoderBase);

#endif /* ENCODER_MR_TYPE_ML_H */
