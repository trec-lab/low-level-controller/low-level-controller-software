/**
 * EncoderMRTypeML.c
 * Contains all of the low-level code for encoder MR type ML realted functions
 */
#include "EncoderMRTypeML.h"
#include "../HAL/QEI_TIVA.h"
#include <stdlib.h>

/**
 * qeiEncoderConstruct
 * Construct a QEI encoder given the corresponding input parameters
 *
 * @param QEIBase: the QEI Base of the encoder
 * @param sampleRate: the sample rate of the QEI encoder
 * @param countsPerRotation: the counts per rotation of the QEI encoder
 *
 * @return: an initialized QEI encoder structure
 */
QuadratureEncoder* EncoderMRTypeMLConstruct(uint32_t QEIBase, uint16_t sampleRate, int32_t countsPerRotation)
{
    static const QuadratureEncoder_vTable vtable =
    {
         EncoderMRTypeMLEnable,
         EncoderMRTypeMLReadPosition,
         EncoderMRTypeMLReadVelocity,
         EncoderMRTypeMLFree
    };

    QuadratureEncoder QuadratureEncoderBase;
    QuadratureEncoderBase.vtable = &vtable;

    // set the QEI Base accordingly
    QuadratureEncoderBase.CommunicationBase = QEIBase;

    // set the sample rate accordingly
    QuadratureEncoderBase.sampleRate = sampleRate;

    // set the counts per rotation accordingly
    QuadratureEncoderBase.countsPerRotation = countsPerRotation;

    // by default the encoder is disabled
    QuadratureEncoderBase.enabled = false;

    // initialize the rest of the values to 0
    QuadratureEncoderBase.raw = 0;
    QuadratureEncoderBase.raw = 0;
    QuadratureEncoderBase.speed = 0;
    QuadratureEncoderBase.direction = 0;
    QuadratureEncoderBase.rawVel = 0;

    EncoderMRTypeML* encoderMRTypeML = (EncoderMRTypeML*)malloc(sizeof(EncoderMRTypeML));
    encoderMRTypeML->ENCODER_BASE = QuadratureEncoderBase;

    // return the new encoder
    return &encoderMRTypeML->ENCODER_BASE;
}

/**
 * EncoderMRTypeMLEnable
 * enables the EncoderMRTypeML Encoder
 *
 * @param encoderBase: a pointer to the base Quadrature encoder to enable
 */
void EncoderMRTypeMLEnable(QuadratureEncoder* encoderBase)
{
    if(encoderBase->CommunicationBase == QEI0_BASE)
        QEI0_EncoderMRTypeML_Config();
    else if(encoderBase->CommunicationBase == QEI1_BASE)
        QEI1_EncoderMRTypeML_Config();

    // set the enabled flag to true
    encoderBase->enabled = true;
}

/**
 * EncoderMRTypeMLReadPosition
 * reads the raw encoder position
 *
 * @param encoderBase: a pointer to the base Quadrature encoder to enable
 */
void EncoderMRTypeMLReadPosition(QuadratureEncoder* encoderBase)
{
    // store the raw value of the encoder
    encoderBase->raw = QEIPositionGet(encoderBase->CommunicationBase);
}

/**
 * EncoderMRTypeMLReadVelocity
 * reads the velocity of the QEI Encoder
 *
 * @param encoderBase: a pointer to the base Quadrature encoder to enable
 */
void EncoderMRTypeMLReadVelocity(QuadratureEncoder* encoderBase)
{
    // get the speed of the encoder
    encoderBase->speed = QEIVelocityGet(encoderBase->CommunicationBase);

    // get the direction of the encoder
    encoderBase->direction = QEIDirectionGet(encoderBase->CommunicationBase);

    // get the raw velocity of the encoder
    encoderBase->rawVel = encoderBase->direction*encoderBase->speed*encoderBase->sampleRate/(encoderBase->countsPerRotation * 4);
}


void EncoderMRTypeMLFree(QuadratureEncoder* encoderBase)
{
    EncoderMRTypeML* encoderMRTypeML = (EncoderMRTypeML*) encoderBase;
    free(encoderMRTypeML);
}
