/**
 * FutekForceSensor.c
 * Contains all of the low-level code for Futek Force Sensor related functions
 */

#include "FutekForceSensor.h"
#include "../HAL/ADC_TIVA.h"
#include <stdlib.h>
/**
 * forceSensorConstruct
 * Constructs a force sensor given the corresponding input parameters
 *
 * @param ADCBase: The ADC base of the force sensor
 * @param ADCSequenceNumber: The number of the ADC sequencer to use
 * @param ADCGPIORawPortBaseLetter: The letter of the GPIO Port Base to use
 * @param ADCGPIORawPinNumber: The pin number of the GPIO Pin to use
 * @param slope: The slope value of the force sensor
 * @param offset: The offset value of the force sensor
 * @param forceUpperLimitNewtons: The upper limit of the force sensor in newtons
 * @param forceLowerLimitNewtons: The lower limit of the force sensor in newtons
 *
 * @return: an initialized force sensor structure
 */
ForceSensor* FutekForceSensorConstruct(uint32_t ADCBase, uint8_t ADCSequenceNumber,
                                 char ADCGPIORawPortBaseLetter, uint8_t ADCGPIORawPinNumber,
                                 float slope, float offset,
                                 float forceUpperLimitNewtons, float forceLowerLimitNewtons)
{
    static const ForceSensor_vTable vtable =
    {
         FutekForceSensorEnable,
         FutekForceSensorReadForce,
         FutekForceSensorFree
    };

    ForceSensor ForceSensorBase;
    ForceSensorBase.vtable = &vtable;

    ForceSensorBase.CommunicationBase = ADCBase;

    ForceSensorBase.upperLimitNewtons = forceUpperLimitNewtons;

    ForceSensorBase.lowerLimitNewtons = forceLowerLimitNewtons;
    ForceSensorBase.raw = 0;

    ForceSensorBase.newtons = 0;
    ForceSensorBase.enabled = false;

    FutekForceSensor* futekForceSensor = (FutekForceSensor*)malloc(sizeof(FutekForceSensor));
    futekForceSensor->FORCESENSOR_BASE = ForceSensorBase;

    futekForceSensor->ADCSequenceNumber = ADCSequenceNumber;

    futekForceSensor->ADCGpioSysCtlPortBase = SYSCTL_PERIPH_GPIOA + (ADCGPIORawPortBaseLetter - 0x41);

    if(ADCGPIORawPortBaseLetter <= 0x44)
        futekForceSensor->ADCGpioPortBase = GPIO_PORTA_BASE + ((1 << 12) * (ADCGPIORawPortBaseLetter - 0x41));
    else
        futekForceSensor->ADCGpioPortBase = GPIO_PORTE_BASE + ((1 << 12) * (ADCGPIORawPortBaseLetter - 0x45));

    futekForceSensor->ADCGpioPinNumber = GPIO_PIN_0 << ADCGPIORawPinNumber;

    if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 3)
        futekForceSensor->ADCChannel = ADC_CTL_CH0;
    else if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 2)
        futekForceSensor->ADCChannel = ADC_CTL_CH1;
    else if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 1)
        futekForceSensor->ADCChannel = ADC_CTL_CH2;
    else if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 0)
        futekForceSensor->ADCChannel = ADC_CTL_CH3;
    else if(ADCGPIORawPortBaseLetter == 'D' && ADCGPIORawPinNumber == 3)
        futekForceSensor->ADCChannel = ADC_CTL_CH4;
    else if(ADCGPIORawPortBaseLetter == 'D' && ADCGPIORawPinNumber == 2)
        futekForceSensor->ADCChannel = ADC_CTL_CH5;
    else if(ADCGPIORawPortBaseLetter == 'D' && ADCGPIORawPinNumber == 1)
        futekForceSensor->ADCChannel = ADC_CTL_CH6;
    else if(ADCGPIORawPortBaseLetter == 'D' && ADCGPIORawPinNumber == 0)
        futekForceSensor->ADCChannel = ADC_CTL_CH7;
    else if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 5)
        futekForceSensor->ADCChannel = ADC_CTL_CH8;
    else if(ADCGPIORawPortBaseLetter == 'E' && ADCGPIORawPinNumber == 4)
        futekForceSensor->ADCChannel = ADC_CTL_CH9;
    else if(ADCGPIORawPortBaseLetter == 'B' && ADCGPIORawPinNumber == 4)
        futekForceSensor->ADCChannel = ADC_CTL_CH10;
    else if(ADCGPIORawPortBaseLetter == 'B' && ADCGPIORawPinNumber == 5)
        futekForceSensor->ADCChannel = ADC_CTL_CH11;

    futekForceSensor->slope = slope;
    futekForceSensor->offset = offset;


    return &futekForceSensor->FORCESENSOR_BASE;
}

/**
 * enableForceSensor
 * Enables the force sensor
 *
 * @param forceSensor: a pointer to the force sensor to enable
 */
void FutekForceSensorEnable(ForceSensor* forceSensorBase)
{
    FutekForceSensor* futekForceSensor = (FutekForceSensor*) forceSensorBase;
    if(forceSensorBase->CommunicationBase == ADC0_BASE)
    {
        ADCConfig0(futekForceSensor->ADCSequenceNumber, futekForceSensor->ADCGpioSysCtlPortBase,
                   futekForceSensor->ADCGpioPortBase, futekForceSensor->ADCGpioPinNumber,
                   futekForceSensor->ADCChannel);
    }
    else if(forceSensorBase->CommunicationBase == ADC1_BASE)
    {
        ADCConfig1(futekForceSensor->ADCSequenceNumber, futekForceSensor->ADCGpioSysCtlPortBase,
                   futekForceSensor->ADCGpioPortBase, futekForceSensor->ADCGpioPinNumber,
                   futekForceSensor->ADCChannel);
    }
    else
        return;

    forceSensorBase->enabled = true;
}

void FutekForceSensorReadForce(ForceSensor* forceSensorBase)
{
    FutekForceSensor* futekForceSensor = (FutekForceSensor*) forceSensorBase;
    // Causes a processor trigger for a sample sequence, trigger the sample sequence.
    ADCProcessorTrigger(forceSensorBase->CommunicationBase, futekForceSensor->ADCSequenceNumber);

    // Wait until the sample sequence has completed.
    // was ADCIntStatusEx
    while(!ADCIntStatus(forceSensorBase->CommunicationBase, futekForceSensor->ADCSequenceNumber, false));

    // Clear the ADC interrupt flag generated upon ADC completion.
    ADCIntClear(forceSensorBase->CommunicationBase, futekForceSensor->ADCSequenceNumber);

    // Obtain single software averaged ADC value.
    ADCSoftwareOversampleDataGet(forceSensorBase->CommunicationBase, futekForceSensor->ADCSequenceNumber, &forceSensorBase->raw, 1);

    forceSensorBase->newtons = forceSensorBase->raw * futekForceSensor->slope + futekForceSensor->offset;
}

void FutekForceSensorFree(ForceSensor* forceSensorBase)
{
    FutekForceSensor* futekForceSensor = (FutekForceSensor*) forceSensorBase;
    free(futekForceSensor);
}

