/**
 * FutekForceSensor.h
 * @author: Nick Tremaroli
 * Contains the low-level code for functions related
 * to the Futek Force Sensor
 */

#ifndef FUTEK_FORCE_SENSOR_H
#define FUTEK_FORCE_SENSOR_H

#include "../ForceSensor.h"

struct FutekForceSensor
{
    ForceSensor FORCESENSOR_BASE;

    // fields specific to the Futek Force Sensor
    uint32_t ADCSequenceNumber;
    uint32_t ADCGpioSysCtlPortBase;
    uint32_t ADCGpioPortBase;
    uint32_t ADCGpioPinNumber;
    uint32_t ADCChannel;

    float slope;
    float offset;
};
typedef struct FutekForceSensor FutekForceSensor;

ForceSensor* FutekForceSensorConstruct(uint32_t ADCBase, uint8_t ADCSequenceNumber,
                                    char ADCGPIORawPortBaseLetter, uint8_t ADCGPIORawPinNumber,
                                    float slope, float offset,
                                    float forceUpperLimitNewtons, float forceLowerLimitNewtons);

void FutekForceSensorEnable(ForceSensor* forceSensorBase);

void FutekForceSensorReadForce(ForceSensor* forceSensorBase);

void FutekForceSensorFree(ForceSensor* forceSensorBase);

#endif /* FUTEK_FORCE_SENSOR_H */
