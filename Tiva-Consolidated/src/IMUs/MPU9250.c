/**
 * MPU9250.c
 * @author: Nick Tremaroli
 * Contains all of the low-level code for MPU9250 related functions
 */

#include "MPU9250.h"
#include <stdlib.h>

/**
 * MPU9250Construct
 * constructs an MPU9250
 */
IMU* MPU9250Construct(uint32_t I2CBase)
{
    static const IMU_vTable vtable =
    {
         MPU9250Enable,
         MPU9250Read,
         MPU9250Free
    };
    IMU IMUBase;

    IMUBase.vtable = &vtable;

    IMUBase.CommunicationBase = I2CBase;

    // set it to disabled by default
    IMUBase.enabled = false;

    // Initialize the raw acceleration data to 0
    IMUBase.accelerationData.AxRaw = 0.0;
    IMUBase.accelerationData.AyRaw = 0.0;
    IMUBase.accelerationData.AzRaw = 0.0;

    // Initialize the acceleration data to 0
    IMUBase.accelerationData.Ax = 0.0;
    IMUBase.accelerationData.Ay = 0.0;
    IMUBase.accelerationData.Az = 0.0;

    // Initialize the raw gyro data to 0
    IMUBase.gyroData.GxRaw = 0;
    IMUBase.gyroData.GyRaw = 0;
    IMUBase.gyroData.GzRaw = 0;

    // Initialize the gyro data to 0
    IMUBase.gyroData.Gx = 0.0;
    IMUBase.gyroData.Gy = 0.0;
    IMUBase.gyroData.Gz = 0.0;

    MPU9250* mpu9250 = (MPU9250*)malloc(sizeof(MPU9250));
    mpu9250->IMU_BASE = IMUBase;

    // Initialize the gyro bias data to 0
    mpu9250->gyroBias.GxBias = 0.0;
    mpu9250->gyroBias.GyBias = 0.0;
    mpu9250->gyroBias.GzBias = 0.0;

    // Initialize the raw magnetometer data to 0
    mpu9250->magnetometerData.MxRaw = 0;
    mpu9250->magnetometerData.MyRaw = 0;
    mpu9250->magnetometerData.MzRaw = 0;

    // Initialize the mangetometer data to 0
    mpu9250->magnetometerData.Mx = 0.0;
    mpu9250->magnetometerData.My = 0.0;
    mpu9250->magnetometerData.Mz = 0.0;

    // Initialize the magnetometer factory bias data to 0
    mpu9250->magnetometerFactoryBias.MxFactoryBias = 0.0;
    mpu9250->magnetometerFactoryBias.MyFactoryBias = 0.0;
    mpu9250->magnetometerFactoryBias.MzFactoryBias = 0.0;

    // Initialize the magnetometer bias data to 0
    mpu9250->magnetometerBias.MxBias = 0.0;
    mpu9250->magnetometerBias.MyBias = 0.0;
    mpu9250->magnetometerBias.MzBias = 0.0;

    // Initialize the magnetometer scale to 0
    mpu9250->magnetometerScaleFactors.MxScale = 0.0;
    mpu9250->magnetometerScaleFactors.MyScale = 0.0;
    mpu9250->magnetometerScaleFactors.MzScale = 0.0;

    // return the newly constructed IMU
    return &mpu9250->IMU_BASE;
}

/**
 * readAK8963Registers
 * Reads data from the registers of the AK8963 (the magnetometer)
 * given the address and the number of bytes to read
 * @param imu: a pointer to the imu to read the AK8963 registers from
 * @param subAddress: the sub-address of the registers located on the AK8963
 * @param length: the number of bytes to read
 * @param dest: a pointer to where the data read will be stored
 */
static void readAK8963Registers(IMU* imuBase, uint8_t subAddress, uint8_t length, uint8_t* dest)
{
    // instruct to talk to the AK8963
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, I2C_SLV0_ADDR, AK8963_ADDRESS | I2C_READ_FLAG);

    // instruct to read from the given sub-address
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, I2C_SLV0_REG, subAddress);

    // instruct to receive a certain number of bytes
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, I2C_SLV0_CTRL, I2C_SLV0_EN | length);

    // wait for it to come back online
    SysCtlDelay(100000);

    // read the data from the AK8963 and store it
    I2C_ReadBytes(imuBase->CommunicationBase, MPU9250_ADDRESS, EXT_SENS_DATA_00, length, dest);

}

/**
 * writeAK8963Register
 * Write data to a register on the AK8963 (the magentometer)
 * give the sub-address to write to
 * @param imu: a pointer to the imu to read the AK8963 registers from
 * @param subAddress: The sub-address of the register on the AK8963
 * @param data: the data to write to the register
 */
static void writeAK8963Register(IMU* imuBase, uint8_t subAddress, uint8_t data)
{
    // wait a little bit before we write data, writing data to fast
    // may result in a failure
    SysCtlDelay(1000000);

    // instruct to talk to the AK8963
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, I2C_SLV0_ADDR, AK8963_ADDRESS);

    // wait a little bit before we write data, writing data to fast
    // may result in a failure
    SysCtlDelay(1000000);

    // instruct to write to the sub-address given
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, I2C_SLV0_REG, subAddress);

    // wait a little bit before we write data, writing data to fast
    // may result in a failure
    SysCtlDelay(1000000);

    // write the data
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, I2C_SLV0_DO, data);

    // wait a little bit before we write data, writing data to fast
    // may result in a failure
    SysCtlDelay(1000000);

    // signal that the write has been completed
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, I2C_SLV0_CTRL, I2C_SLV0_EN | (uint8_t)1);

    // wait a little bit before we write data, writing data to fast
    // may result in a failure
    SysCtlDelay(1000000);

    // read from the AK8963 to make sure the data was successfully written
    uint8_t result;
    readAK8963Registers(imuBase, subAddress, 1, &result);
}

/**
 * imuInitialize
 * Initializes all of the IMU peripherals:
 * accelerometer, gyro, mangetometer
 * @param imu: a pointer to the IMU to initialize
 */
static void MPU9250Initialize(IMU* imuBase)
{
    MPU9250* mpu9250 = (MPU9250*) imuBase;

    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, PWR_MGMNT_1, CLOCK_SEL_PLL);

    // Enable I2C Master mode
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, USER_CTRL, I2C_MST_EN);

    // set I2C bus speed to 400 KHz
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, I2C_MST_CTRL, I2C_MST_CLK);

    // set AK8963 to power down
    writeAK8963Register(imuBase, AK8963_CNTL1, AK8963_PWR_DOWN);

    // reset the MPU9250
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, PWR_MGMNT_1, PWR_RESET);

    // wait for it to come back online
    SysCtlDelay(1000000);

    // reset the AK8963
    writeAK8963Register(imuBase, AK8963_CNTL2, AK8963_RESET);

    // select clock source to gyro
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, PWR_MGMNT_1, CLOCK_SEL_PLL);

    // enable the accelerometer and the gyro
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, PWR_MGMNT_2, SEN_ENABLE);

    // set accelerometer range to 16G by default
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, ACCEL_CONFIG, ACCEL_FS_SEL_16G);

    // set gyro range to 2000DPS
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, GYRO_CONFIG, GYRO_FS_SEL_2000DPS);

    // set acceleration bandwidth to 182Hz by default
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, ACCEL_CONFIG2, ACCEL_DLPF_184);

    // set gyro bandwith to 182Hz by default
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, CONFIG, GYRO_DLPF_184);

    // set sample rate divider to 0 as default
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, SMPDIV, 0x00);

    // enable I2C master mode
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, USER_CTRL, I2C_MST_EN);

    // set I2C bus speed to 400 Khz
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, I2C_MST_CTRL, I2C_MST_CLK);

    // check connectiont o the AK8962, should return 72 in decimal
    uint8_t result = 0;
    readAK8963Registers(imuBase, WHO_AM_I_AK8963, 1 ,&result);

    // get magnetometer calibration, power down
    writeAK8963Register(imuBase, AK8963_CNTL1, AK8963_PWR_DOWN);

    // wait for it to come back online
    SysCtlDelay(1000000);

    // set AK8963 to FUSE ROM access
    writeAK8963Register(imuBase, AK8963_CNTL1, AK8963_FUSE_ROM);

    // wait for the change to occur
    SysCtlDelay(1000000);

    // read the raw factory bias values from the AK8963 (the mangeotmeter) to
    uint8_t results[3];
    readAK8963Registers(imuBase, AK8963_ASA, 3, results);

    // store the factory bias values accordingly (values in micro Telsa after conversion)
    mpu9250->magnetometerFactoryBias.MxFactoryBias = ((((float)results[0]) - 128.0f)/(256.0f) + 1.0f) * 4912.0f / 32760.0f;
    mpu9250->magnetometerFactoryBias.MyFactoryBias = ((((float)results[1]) - 128.0f)/(256.0f) + 1.0f) * 4912.0f / 32760.0f;
    mpu9250->magnetometerFactoryBias.MzFactoryBias = ((((float)results[2]) - 128.0f)/(256.0f) + 1.0f) * 4912.0f / 32760.0f;

    // set AK8963 to power down
    writeAK8963Register(imuBase, AK8963_CNTL1, AK8963_PWR_DOWN);

    // set AK8963 to 16 bit resolution, 100Hz update rate
    writeAK8963Register(imuBase, AK8963_CNTL1, AK8963_CNT_MEAS2);

    // wait for mode change
    SysCtlDelay(1000000);

    // select clock source for gyro
    I2C_WriteByte(imuBase->CommunicationBase, MPU9250_ADDRESS, PWR_MGMNT_1, CLOCK_SEL_PLL);

    // instruct the AK8963 (the magnetometer) to read 7 byte of
    // data to complete initialization
    uint8_t buffer[7];
    readAK8963Registers(imuBase, 0x03, 7, &buffer[0]);
}

/**
 * imuCalibrate
 * Calibrates the imu and addresses its initial biases. Calibrations involves
 * taking multiple reading iterations to do.
 */
static void MPU9250Calibrate(IMU* imuBase)
{
    MPU9250* mpu9250 = (MPU9250*) imuBase;

    // Initialize the temporary bias variables for gyro
    float gyrobiasX = 0.0;
    float gyrobiasY = 0.0;
    float gyrobiasZ = 0.0;

    // take 100 samples
    uint16_t i;
    int iter = 100;
    for(i = 0; i < iter; i++)
    {
        // read the Sensor Data
        MPU9250Read(imuBase);

        // store the gyro bias for this iteration
        gyrobiasX += imuBase->gyroData.Gx;
        gyrobiasY += imuBase->gyroData.Gy;
        gyrobiasZ += imuBase->gyroData.Gz;
    }

    // take an average to store the bias
    mpu9250->gyroBias.GxBias = -1.0 * (gyrobiasX/(iter + 1));
    mpu9250->gyroBias.GyBias = -1.0 * (gyrobiasY/(iter + 1));
    mpu9250->gyroBias.GzBias = -1.0 * (gyrobiasZ/(iter + 1));
}


/**
 * MPU9250Enable
 * enables the IMU on the Tiva
 * @param: imu: a pounter to the imu to enable
 */
void MPU9250Enable(IMU* imuBase)
{
    // enable the I2C pins used
    if(imuBase->CommunicationBase == I2C0_BASE)
        I2C0_Config();
    else if(imuBase->CommunicationBase == I2C1_BASE)
        I2C1_Config();
    else if(imuBase->CommunicationBase == I2C2_BASE)
        I2C2_Config();
    else if(imuBase->CommunicationBase == I2C3_BASE)
        I2C3_Config();

    MPU9250Initialize(imuBase);

    // sets the biases
    MPU9250Calibrate(imuBase);

    imuBase->enabled = true;
}

/**
 * readSensorData
 * reads all the necessary sensor data from the IMU:
 * acceleration data, gyro data, magentometer data
 * @param imu: The IMU to read data from
 */
void MPU9250Read(IMU* imuBase)
{
    MPU9250* mpu9250 = (MPU9250*) imuBase;

    // read the raw data from the IMU
    uint8_t rawByteData[21];
    I2C_ReadBytes(imuBase->CommunicationBase, MPU9250_ADDRESS, ACCEL_OUT, 21, rawByteData);

    // store the raw accceleration data
    imuBase->accelerationData.AxRaw = (((int16_t)rawByteData[0]) << 8) | rawByteData[1];
    imuBase->accelerationData.AyRaw = (((int16_t)rawByteData[2]) << 8) | rawByteData[3];
    imuBase->accelerationData.AzRaw = (((int16_t)rawByteData[4]) << 8) | rawByteData[5];

    // store the raw gyro data
    imuBase->gyroData.GxRaw = (((int16_t)rawByteData[8]) << 8) | rawByteData[9];
    imuBase->gyroData.GyRaw = (((int16_t)rawByteData[10]) << 8) | rawByteData[11];
    imuBase->gyroData.GzRaw = (((int16_t)rawByteData[12]) << 8) | rawByteData[13];

    // store the raw magnetometer data
    mpu9250->magnetometerData.MxRaw = (((int16_t)rawByteData[15]) << 8) | rawByteData[14];
    mpu9250->magnetometerData.MyRaw = (((int16_t)rawByteData[17]) << 8) | rawByteData[16];
    mpu9250->magnetometerData.MzRaw = (((int16_t)rawByteData[19]) << 8) | rawByteData[18];

    // Convert the raw acceleration data to the actual accleration data
    imuBase->accelerationData.Ax = (float)(imuBase->accelerationData.AxRaw) * ACCELEROMETERFACTOR_16G;
    imuBase->accelerationData.Ay = (float)(imuBase->accelerationData.AyRaw) * ACCELEROMETERFACTOR_16G;
    imuBase->accelerationData.Az = (float)(imuBase->accelerationData.AzRaw) * ACCELEROMETERFACTOR_16G;

    // Convert the raw gyro data to the actual gyro data
    imuBase->gyroData.Gx = ((float)(imuBase->gyroData.GxRaw) * GYROFACTOR_2000DPS) + mpu9250->gyroBias.GxBias;
    imuBase->gyroData.Gy = ((float)(imuBase->gyroData.GyRaw) * GYROFACTOR_2000DPS) + mpu9250->gyroBias.GyBias;
    imuBase->gyroData.Gz = ((float)(imuBase->gyroData.GzRaw) * GYROFACTOR_2000DPS) + mpu9250->gyroBias.GzBias;

    // Convert the raw magnetometer data to actual magnetometer data
    mpu9250->magnetometerData.Mx = ((float)mpu9250->magnetometerData.MxRaw) * mpu9250->magnetometerFactoryBias.MxFactoryBias;
    mpu9250->magnetometerData.My = ((float)mpu9250->magnetometerData.MyRaw) * mpu9250->magnetometerFactoryBias.MyFactoryBias;
    mpu9250->magnetometerData.Mz = ((float)mpu9250->magnetometerData.MzRaw) * mpu9250->magnetometerFactoryBias.MzFactoryBias;
}

void MPU9250Free(IMU* imuBase)
{
    MPU9250* mpu9250 = (MPU9250*) imuBase;
    free(mpu9250);
}


