/**
 * AbsooluteEncoder.h
 * @author: Nick Tremaroli
 * Contains the layout and functions regarding an Absolute Encoder
 */
#ifndef ABSOLUTE_ENCODER_H
#define ABSOLUTE_ENCODER_H

#include <stdint.h>
#include <stdbool.h>

enum AbsoluteEncoderBrand
{
    Gurley_AbsoluteEncoder,
    Orbis_AbsoluteEncoder
};
typedef enum AbsoluteEncoderBrand AbsoluteEncoderBrand;

struct AbsoluteEncoder;

/**
 * AbsoluteEncoder_vTable
 * Contains all of the required functions to be inherited
 * by the derived Absolute Encoders
 */
struct AbsoluteEncoder_vTable
{
    void (*AbsoluteEncoderEnable)(struct AbsoluteEncoder*);
    void (*AbsoluteEncoderReadSensor)(struct AbsoluteEncoder*);
    void (*AbsoluteEncoderFree)(struct AbsoluteEncoder*);
};
typedef struct AbsoluteEncoder_vTable AbsoluteEncoder_vTable;

/**
 * AbsoluteEncoder
 * Contains all of the data needed by an absolute encoder
 * on the Tiva
 */
struct AbsoluteEncoder
{
    // the virtual table which allows polymorphism to happen
    const AbsoluteEncoder_vTable* vtable;

    // SSI0_Base, SSI1_Base, IC20_BASE, I2C1_BASE etc.
    uint32_t CommunicationBase;

    uint16_t sampleRate;
    bool enabled;

    // the raw values
    uint32_t raw;
    uint32_t rawPrev;
    int32_t rawVelF;
    int32_t rawVelPrev;
    int32_t rawVel;

    uint32_t rawPi;

};
typedef struct AbsoluteEncoder AbsoluteEncoder;


void AbsoluteEncoderEnable(AbsoluteEncoder* encoder);

void AbsoluteEncoderRead(AbsoluteEncoder* encoder);

void AbsoluteEncoderFree(AbsoluteEncoder* encoder);

#endif /* ABSOLTE_ENCODER_H */
