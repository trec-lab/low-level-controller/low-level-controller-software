/**
 * SystemLED.h
 * Contains the layount and functions for the system LED
 * They System LED is an LED which can be used to communicate error
 * codes before EtherCAT communication is established and thus error codes cannot be sent back
 * to the Master.
 * The System LED should only be initialized if an error occurs before initialization
 * is completed
 */

#ifndef SYSTEMLED_H
#define SYSTEMLED_H

#include "StatusLEDs.h"

// the delays for the system LEDs
#define SMALLDELAY 10000000
#define LARGEDELAY 80000000

// the blink code for when the EasyCAT reaches a reset timeout
void StatusEasyCATResetTimeout(LED* statusLED);

// the blink code for when the EasyCAT has a byte order failure
void StatusEasyCATByteOrderFailed(LED* statusLED);

// the blink code for when the EasyCAT has a ready flag timeout error
void StatusEasyCATReadyFlagTimeout(LED* statusLED);

// the blink code for when the error status of the EasyCAT is unknown
void StatusEasyCATUnknown(LED* statusLED);

#endif /* SYSTEMLED_H */
