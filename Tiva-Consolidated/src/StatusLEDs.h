/**
 * StatusLEDs.h
 * @author: Nick Tremaroli
 * Contains the layout and functions regarding status LEDs.
 * The LED is particularly used for debugging purposes and is lit to be
 * a certain color according to the status of the Tiva board
 */

#ifndef STATUSRGBLED_H
#define STATUSRGBLED_H

#include <inttypes.h>
#include <stdbool.h>

#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"

#define LED_ON_DELAY_TIME 1000
#define LED_OFF_DELAY_TIME 500

/**
 * LED
 *
 * a structure which contains the basic
 * data needed to enable and use an LED
 */
struct LED
{
    uint32_t systemControlPeripheralBase;
    uint32_t GPIOBase;
    uint32_t GPIOPin;
};
typedef struct LED LED;

/**
 * RGBLED
 *
 * a structure containing 3 LEDs
 * for an RGB LED
 */
struct RGBLED
{
    LED red;
    LED green;
    LED blue;
};
typedef struct RGBLED RGBLED;

// construct an LED
LED ledConstruct(uint32_t systemControlPeripheralBase, uint32_t GPIOBase, uint32_t GPIOPin);

// construct an RGB LED
RGBLED rgbLEDConstruct(uint32_t redSystemControlPeripheralBase, uint32_t redGPIOBase, uint32_t redGPIOPin,
                       uint32_t greenSystemControlPeripheralBase, uint32_t greenGPIOBase, uint32_t greenGPIOPin,
                       uint32_t blueSystemControlPeripheralBase, uint32_t blueGPIOBase, uint32_t blueGPIOPin);

// enable an LED
void enableLED(LED* led);

// enable an RGB LED
void enableRGBLED(RGBLED* rgbLed);

// turn on an LED
void ledTurnOn(LED* led);

// turn off an LED
void ledTurnOff(LED* led);

// LED patterns for when the Tiva is not connected to the master
void notConnectedLEDS(RGBLED* rgbLed);

// LED patterns for when the Tiva is in idle mode
void idleLEDS(RGBLED* rgbLed);

// LED patterns for when the Tiva is in halt mode
void haltLEDS(RGBLED* rgbLed);

// LED patterns for when the Tiva is in control mode
void controlLEDS(RGBLED* rgbLed);

#endif
