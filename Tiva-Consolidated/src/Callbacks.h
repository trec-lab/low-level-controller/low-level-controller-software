/*
 * Callbacks.h
 *
 *  Created on: Oct 5, 2022
 *      Author: Max Stelmack
 */

#ifndef CALLBACKS_H_
#define CALLBACKS_H_

//*****************************************************************************
//
//includes
//

// Standard library
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

// TivaWare
#include "inc/hw_ints.h"
#include "inc/hw_ssi.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/timer.h"
#include "driverlib/qei.h"
#include "driverlib/ssi.h"
#include "driverlib/adc.h"
#include "driverlib/pwm.h"
#include "utils/uartstdio.h"

// HAL
#include "HAL/LAN9252_TI_TIVA.h"
#include "HAL/Timer_TIVA.h"
#include "HAL/UART_TIVA.h"
#include "HAL/CAN_TI_TIVA.h"

// Implementation
#include "EtherCAT_FrameData.h"
#include "TivaLowLevel.h"
#include "SystemLED.h"
#include "HaltCodes.h"

//*****************************************************************************
//
// functions prototypes
//

void lowLevelStartup(void);
void lowLevelStartup_noMaster(void);
void readSensors();
void checkEstop(void);

#endif /* CALLBACKS_H_ */
