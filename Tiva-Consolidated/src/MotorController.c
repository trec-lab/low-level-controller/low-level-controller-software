#include "MotorController.h"

/**
 * MotorControllerConstruct
 * Generators and initializes a MotorController struct
 * @param pwmFrequency: The frequency the PWM will run at
 * on the motor controller
 */
MotorController MotorControllerConstruct(uint16_t pwmFrequency)
{
    // create the PWM generator
    MotorController motorController;

    // initialize direction and duty cycle to zero
    motorController.direction = 0;
    motorController.dutyCycle = 0.0;

    // set the pwm Frequency accordingly
    motorController.pwmFrequency = pwmFrequency;

    // return the newly constructed PWM generator
    return motorController;
}

/**
 * SetPulseWidth
 * Sets the pulse width of a PWM pin given the appropriate input parameters
 * @param actuator: a number corresponding to which side of the sensor board to
 * send a pwm signal (actuator 0 or actuator 1)
 * @param motorController: a pointer to the motor controller which contains
 * all of the settings on how to command the pwm
 * @param sysClock: The input clock speed of the Tiva
 */
void setPulseWidth(uint8_t actuator, MotorController* motorController, uint32_t sysClock)
{
    // get the pwmClockSpeed
    uint32_t pwmClockSpeed = sysClock / 2;

    // Set PWM dead zone
    if (motorController->dutyCycle < 0.1)
        motorController->dutyCycle = 0;

    // prevent the pwm from going over the maximum value
    else if(motorController->dutyCycle >= 100.0)
        motorController->dutyCycle = 100.0;

    //Added SysCtlPeripheralEnable for GPIO and PWM, Modified the PWM generator in the Configure and PeriodSet to generator 0
    SysCtlPWMClockSet(SYSCTL_PWMDIV_2);

    // TODO: Remove this and replace it with a better method
    // switch case statement for each actuator number
    switch(actuator)
    {

    // Actuator 0
    case 0:

        //Configure the PWM generator for count down mode with immediate updates to the parameters.
        PWMGenConfigure(PWM0_BASE, PWM_GEN_2, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);

        //Set the period of the pwm signal using the pwm clock frequency and the desired signal frequency
        PWMGenPeriodSet(PWM0_BASE, PWM_GEN_2, (int)((float)pwmClockSpeed/(float)motorController->pwmFrequency));

        //Set the pulse width of PWM2 with duty cycle. PWM_OUT_2 refers to the second PWM2.
        PWMPulseWidthSet(PWM0_BASE, PWM_OUT_4, (int)(((float)pwmClockSpeed/(float)motorController->pwmFrequency)*((float)motorController->dutyCycle/100)));

        // Enable the PWM
        PWMGenEnable(PWM0_BASE, PWM_GEN_2);

        // Output the pwm duty cycle
        if (motorController->dutyCycle == 0)
           PWMOutputState(PWM0_BASE, (PWM_OUT_4_BIT), false);
        else
            PWMOutputState(PWM0_BASE, (PWM_OUT_4_BIT), true);

        // Output the direction pin
        // Direction is Clockwise
        if (motorController->direction == 0)
            GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, 0);

        // Direction is Counter-Clockwise
        else
            GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, 0xff);

        break;

    // Actuator 1
    case 1:

        // Configure the PWM generator for count down mode with immediate updates to the parameters.
        PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);

        // Set the period of the pwm signal using the pwm clock frequency and the desired signal frequency
        PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, (int)((float)pwmClockSpeed/(float)motorController->pwmFrequency));

        // Set the pulse width of PWM2 with duty cycle. PWM_OUT_2 refers to the second PWM2.
        PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0, (int)(((float)pwmClockSpeed/(float)motorController->pwmFrequency)*((float)motorController->dutyCycle/100)));

        // Enable the PWM
        PWMGenEnable(PWM0_BASE, PWM_GEN_0);

        // Output the pwm duty cycle
        if (motorController->dutyCycle == 0)
            PWMOutputState(PWM0_BASE, (PWM_OUT_0_BIT), false);
        else
            PWMOutputState(PWM0_BASE, (PWM_OUT_0_BIT), true);

        // Output the direction pin
        // Direction is Clockwise
        if (motorController->direction == 0)
            GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0);

        // Direction is Counter-Clockwise
        else
            GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0xff);

        break;
      }
}
