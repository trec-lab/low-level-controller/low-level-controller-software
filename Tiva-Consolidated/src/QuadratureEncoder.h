/*
 * QEIEncoder.h
 * @author: Nick Tremaroli
 * Contains the layout and functions regarding a QEI Encoder
 */

#ifndef QUADRATURE_ENCODER_H_
#define QUADRATURE_ENCODER_H_

#include <stdint.h>
#include <stdbool.h>

enum QuadratureEncoderBrand
{
    EncoderMRTypeML_QuadratureEncoder
};
typedef enum QuadratureEncoderBrand QuadratureEncoderBrand;

struct QuadratureEncoder;

struct QuadratureEncoder_vTable
{
    void (*QuadratureEncoderEnable)(struct QuadratureEncoder*);
    void (*QuadratureEncoderReadPosition)(struct QuadratureEncoder*);
    void (*QuadratureEncoderReadVelocity)(struct QuadratureEncoder*);
    void (*QuadratureEncoderFree)(struct QuadratureEncoder*);
};
typedef struct QuadratureEncoder_vTable QuadratureEncoder_vTable;

/**
 * QuadratureEncoder
 * Contains all of the data needed by a Quadrature Encoder
 * on the Tiva
 */
struct QuadratureEncoder
{
    // the virtual table which allows polymorphism to happen
    const QuadratureEncoder_vTable* vtable;

    // the communication base of the encoder
    uint32_t CommunicationBase;

    // the sample rate of the QEI encoder
    uint16_t sampleRate;

    // the raw value of the encoder
    uint32_t raw;

    // the speed of the encoder
    int32_t speed;

    // the direction of the encoder
    int32_t direction;

    // the raw velocity of the encoder
    int32_t rawVel;

    // the counts per rotation of the encoder
    int32_t countsPerRotation;

    // a flag to determine if the
    // encoder is enabled
    bool enabled;
};
typedef struct QuadratureEncoder QuadratureEncoder;

// enable the QEI encoder
void QuadratureEncoderEnable(QuadratureEncoder* encoder);

// get the position of the QEI encoder
void QuadratureEncoderReadPosition(QuadratureEncoder* encoder);

// get the velocity of the QEI encoder
void QuadratureEncoderReadVelocity(QuadratureEncoder* encoder);

void QuadratureEncoderFree(QuadratureEncoder* encoder);

#endif /* QUADRATURE_ENCODER_H_ */
