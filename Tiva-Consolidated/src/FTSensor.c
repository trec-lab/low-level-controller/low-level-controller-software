/**
 * FTSensor.c
 * @author: Nick Tremaroli
 * Contains all of the low-level code for FT sensor related functions
 */

#include "FTSensor.h"

void FTSensorEnable(FTSensor* ftSensor)
{
    ftSensor->vtable->FTSensorEnable(ftSensor);
}

void FTSensorRead(FTSensor* ftSensor)
{
    ftSensor->vtable->FTSensorRead(ftSensor);
}

void FTSensorFree(FTSensor* ftSensor)
{
    ftSensor->vtable->FTSensorFree(ftSensor);
}
