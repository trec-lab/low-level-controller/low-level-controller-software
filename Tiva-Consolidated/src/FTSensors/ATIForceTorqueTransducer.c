/**
 * ATIForceTorqueTransducer.c
 * Contains all of the low-level code for ATI Force Torque Transducer related functions
 */

#include "ATIForceTorqueTransducer.h"
#include <stdlib.h>

/**
 * ftSensorConstruct
 * Constructs an FT sensor and initializes
 * all of the values accordingly
 */
FTSensor* ATIForceTorqueTransducerConstruct(uint32_t canRate, uint32_t canBase,
                                           char canPortBaseLetter, FTSensorLimits sensorLimits)
{
    static const FTSensor_vTable vtable =
    {
         ATIForceTorqueTransducerEnable,
         ATIForceTorqueTransducerRead,
         ATIForceTorqueTransducerFree,
    };

    FTSensor ftSensorBase;
    ftSensorBase.vtable = &vtable;
    ftSensorBase.CommunicationBase = canBase;

    // initialize the force values to 0
    ftSensorBase.forceX = 0.0;
    ftSensorBase.forceY = 0.0;
    ftSensorBase.forceZ = 0.0;

    // initialize the torque values to 0
    ftSensorBase.torqueX = 0.0;
    ftSensorBase.torqueY = 0.0;
    ftSensorBase.torqueZ = 0.0;

    ftSensorBase.limits = sensorLimits;

    ftSensorBase.enabled = false;

    ATIForceTorqueTransducer* atiForceTorqueTransducer = (ATIForceTorqueTransducer*)malloc(sizeof(ATIForceTorqueTransducer));
    atiForceTorqueTransducer->FTSENSOR_BASE = ftSensorBase;

    atiForceTorqueTransducer->canRate = canRate;
    atiForceTorqueTransducer->canPortBaseLetter = canPortBaseLetter;


    // initialize the force bias values to 0
    atiForceTorqueTransducer->bias.forceXBias = 0.0;
    atiForceTorqueTransducer->bias.forceYBias = 0.0;
    atiForceTorqueTransducer->bias.forceZBias = 0.0;

    // initialize the torque bias values to 0
    atiForceTorqueTransducer->bias.torqueXBias = 0.0;
    atiForceTorqueTransducer->bias.torqueYBias = 0.0;
    atiForceTorqueTransducer->bias.torqueZBias = 0.0;

    return &atiForceTorqueTransducer->FTSENSOR_BASE;
}

/**
 * ftSensorCalibrate
 * calibrates the FT sensor and sets the
 * biases accordingly
 * @param ftSensor: a pointer to the FT sensor
 * to calibrate
 */
static void ATIForceTorqueTransducerCalibrate(FTSensor* ftSensorBase)
{
    ATIForceTorqueTransducer* atiForceTorqueTransducer = (ATIForceTorqueTransducer*) ftSensorBase;

    // Read data from the FT Sensor
    ATIForceTorqueTransducerRead(ftSensorBase);

    // set the force biases
    atiForceTorqueTransducer->bias.forceXBias = -1.0 * ftSensorBase->forceX;
    atiForceTorqueTransducer->bias.forceYBias = -1.0 * ftSensorBase->forceY;
    atiForceTorqueTransducer->bias.forceZBias = -1.0 * ftSensorBase->forceZ;

    // set the torque biases
    atiForceTorqueTransducer->bias.torqueXBias = -1.0 * ftSensorBase->torqueX;
    atiForceTorqueTransducer->bias.torqueYBias = -1.0 * ftSensorBase->torqueY;
    atiForceTorqueTransducer->bias.torqueZBias = -1.0 * ftSensorBase->torqueZ;

}

/**
 * SendFTSensorData
 * Sends data to the FT sensor which requests the
 * latest force and torque data
 * @param ftSensor: the FT sensor to send data too
 */
static void ATIForceTorqueTransducerRequestData(FTSensor* ftSensorBase)
{
    ATIForceTorqueTransducer* atiForceTorqueTransducer = (ATIForceTorqueTransducer*) ftSensorBase;
    CANSend(&atiForceTorqueTransducer->TxData0);
}

/**
 * readForceTorqueData
 * reads force and torque data from the FT sensor,
 * computes the output using the biases and stores it accordingly
 * @param ftSensor: the FT sensor to read force torque data from
 */
static void ATIForceTorqueTransducerReadForceTorques(FTSensor* ftSensorBase)
{
    ATIForceTorqueTransducer* atiForceTorqueTransducer = (ATIForceTorqueTransducer*) ftSensorBase;
    // Read from the CAN bus
    ReadCAN(&atiForceTorqueTransducer->RxData0);
    ReadCAN(&atiForceTorqueTransducer->RxData1);

    // store the force value in the X direction
    ftSensorBase->forceX = (BinaryToDecimal(
            atiForceTorqueTransducer->RxData0.msgRxData[0],
            atiForceTorqueTransducer->RxData0.msgRxData[1]) /
            ATI_FORCE_TORQUE_TRANSDUCER_FORCE_FACTOR) + atiForceTorqueTransducer->bias.forceXBias;

    // store the torque value in the X direction
    ftSensorBase->torqueX = (BinaryToDecimal(
            atiForceTorqueTransducer->RxData0.msgRxData[2],
            atiForceTorqueTransducer->RxData0.msgRxData[3]) /
            ATI_FORCE_TORQUE_TRANSDUCER_TORQUE_FACTOR) + atiForceTorqueTransducer->bias.torqueXBias;

    // store the force value in the Y direction
    ftSensorBase->forceY = (BinaryToDecimal(
            atiForceTorqueTransducer->RxData0.msgRxData[4],
            atiForceTorqueTransducer->RxData0.msgRxData[5]) /
                ATI_FORCE_TORQUE_TRANSDUCER_FORCE_FACTOR) + atiForceTorqueTransducer->bias.forceYBias;

    // store the torque value in the Y direction
    ftSensorBase->torqueY = (BinaryToDecimal(
            atiForceTorqueTransducer->RxData0.msgRxData[6],
            atiForceTorqueTransducer->RxData0.msgRxData[7]) /
                ATI_FORCE_TORQUE_TRANSDUCER_TORQUE_FACTOR) + atiForceTorqueTransducer->bias.torqueYBias;

    // store the force value in the Z direction
    ftSensorBase->forceZ = (BinaryToDecimal(
            atiForceTorqueTransducer->RxData1.msgRxData[0],
            atiForceTorqueTransducer->RxData1.msgRxData[1]) /
                ATI_FORCE_TORQUE_TRANSDUCER_FORCE_FACTOR) + atiForceTorqueTransducer->bias.forceZBias;

    // store the torque value in the Z direction
    ftSensorBase->torqueZ = (BinaryToDecimal(
            atiForceTorqueTransducer->RxData1.msgRxData[2],
            atiForceTorqueTransducer->RxData1.msgRxData[3]) /
                ATI_FORCE_TORQUE_TRANSDUCER_TORQUE_FACTOR) + atiForceTorqueTransducer->bias.torqueZBias;
}


/**
 * ftSensorEnable
 * enables the FT sensor and the corresponding
 * CAN Tx and Rx components
 */
void ATIForceTorqueTransducerEnable(FTSensor* ftSensorBase)
{
    ATIForceTorqueTransducer* atiForceTorqueTransducer = (ATIForceTorqueTransducer*) ftSensorBase;

    // initialize CAN given the CAN rate
    if(ftSensorBase->CommunicationBase == CAN0_BASE && atiForceTorqueTransducer->canPortBaseLetter == 'B')
        CAN0_PortB_Config(atiForceTorqueTransducer->canRate);
    else if(ftSensorBase->CommunicationBase == CAN0_BASE && atiForceTorqueTransducer->canPortBaseLetter == 'E')
        CAN0_PortE_Config(atiForceTorqueTransducer->canRate);
    else if(ftSensorBase->CommunicationBase == CAN0_BASE && atiForceTorqueTransducer->canPortBaseLetter == 'F')
        CAN0_PortF_Config(atiForceTorqueTransducer->canRate);

    // Initialize Rx0 to receive data from the FT board
    CANRx0Initial(ftSensorBase->CommunicationBase, &atiForceTorqueTransducer->RxData0, 0x1b5, 2);

    // Initialize Rx1 to receive data from the FT board
    CANRx1Initial(ftSensorBase->CommunicationBase, &atiForceTorqueTransducer->RxData1, 0x1b6, 3);

    // Initialize Tx0 to send data to the FT board
    CANTxInitial(ftSensorBase->CommunicationBase, &atiForceTorqueTransducer->TxData0, 0x1b0, 1);

    // send and get an initial response
    // before data can be read consistently
    ATIForceTorqueTransducerRead(ftSensorBase);

    SysCtlDelay(100000);

    // calibrate the FT sensor
    ATIForceTorqueTransducerCalibrate(ftSensorBase);

    ftSensorBase->enabled = true;
}

void ATIForceTorqueTransducerRead(FTSensor* ftSensorBase)
{
    ATIForceTorqueTransducerRequestData(ftSensorBase);
    ATIForceTorqueTransducerReadForceTorques(ftSensorBase);
}

void ATIForceTorqueTransducerFree(FTSensor* ftSensorBase)
{
    ATIForceTorqueTransducer* atiForceTorqueTransducer = (ATIForceTorqueTransducer*) ftSensorBase;
    free(atiForceTorqueTransducer);
}

