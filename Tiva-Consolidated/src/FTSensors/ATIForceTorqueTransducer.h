/**
 * ATIForceTorqueTransducer.h
 * @author: Nick Tremaroli
 * Contains the low-level code for functions related
 * to the ATI Force Torque Transducer
 */

#ifndef ATI_FORCE_TORQUE_TRANSDUCER_H
#define ATI_FORCE_TORQUE_TRANSDUCER_H

#include "../FTSensor.h"
#include "../HAL/CAN_TI_TIVA.h"

// These factor is hardware configuration in the Net FT board
// use these factors to compute the force and torque respectively
#define ATI_FORCE_TORQUE_TRANSDUCER_TORQUE_FACTOR   (1000000.0 / 611.0)
#define ATI_FORCE_TORQUE_TRANSDUCER_FORCE_FACTOR    (1000000.0 / 35402.0)

struct ATIForceTorqueTransducer
{
    FTSensor FTSENSOR_BASE;

    uint32_t canRate;
    char canPortBaseLetter;
    FTSensorBias bias;

    CANBUSRx RxData0;
    CANBUSRx RxData1;

    CANBUSTx TxData0;
};
typedef struct ATIForceTorqueTransducer ATIForceTorqueTransducer;

// constructs an ATI Force Torque Transducer sensor
FTSensor* ATIForceTorqueTransducerConstruct(uint32_t canRate, uint32_t canBase, char canPortBaseLetter,
                           FTSensorLimits sensorLimits);

void ATIForceTorqueTransducerEnable(FTSensor* ftSensorBase);

void ATIForceTorqueTransducerRead(FTSensor* ftSensorBase);

void ATIForceTorqueTransducerFree(FTSensor* ftSensorBase);

#endif /* ATI_FORCE_TORQUE_TRANSDUCER_H */
