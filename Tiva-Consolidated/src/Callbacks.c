/*
 * Callbacks.c
 *
 *  Created on: Oct 5, 2022
 *      Author: Max Stelmack and Nick Tremaroli
 */
#include "Callbacks.h"

Tiva tiva;

volatile bool operationEnabled = true;
volatile bool runCommunicationLoop = true;


bool EngageVirtualEStop(Tiva* tiva);

void lowLevelStartup(void)
{
    // Populate pandora object
    tiva = tivaConstruct();

    // Initialize tiva
    uint8_t init = tivaInitEtherCAT(&tiva);
    // if an error occurred during the initialization of EtherCAT
    if(init != 0)
    {
        LED systemLED = ledConstruct(SYSCTL_PERIPH_GPIOB, GPIO_PORTB_BASE, GPIO_PIN_0);
        enableLED(&systemLED);
        void (*statusFunction)(LED*);
        if(init == 1)
            statusFunction = &StatusEasyCATResetTimeout;
        else if(init == 2)
            statusFunction = &StatusEasyCATByteOrderFailed;
        else if(init == 3)
            statusFunction = &StatusEasyCATReadyFlagTimeout;
        else
            statusFunction = &StatusEasyCATUnknown;

        // loop forever
        while(1)
            statusFunction(&systemLED);
    }

    while(!tiva.initialized)
    {
        ReceiveFromEasyCAT(&tiva.easyCAT.etherCATInputFrames);
        tiva.prevProcessIdFromMaster = tiva.processIdFromMaster;
        tiva.processIdFromMaster = tiva.easyCAT.etherCATInputFrames.rawBytes[PROCESS_ID_INDEX];

        if(tiva.processIdFromMaster != tiva.prevProcessIdFromMaster)
        {
            storeDataFromMaster(&tiva);
            processDataFromMaster(&tiva);
            loadDataForMaster(&tiva);
        }
        SendToEasyCAT(&tiva.easyCAT.etherCATOutputFrames);

        SysCtlDelay(2000);
    }

    tivaInit(&tiva);

    tiva.initializedOnce = true;

    // Enable processor interrupts
    IntMasterEnable();
}

/**
 * For testing without Master
 */
void lowLevelStartup_noMaster(void)
{
    // Populate pandora object
    tiva = tivaConstruct();

    uint8_t actuatorNumber = 0;

    uint8_t rawQEIBase = 0;
    uint32_t actuator0_QEIBase = QEI0_BASE + ((1 << 12) * rawQEIBase);

    uint16_t actuator0_QEISampleRate = 1000;
    uint32_t actuator0_QEICountsPerRotation = 1000;

    uint8_t rawADCBase = 0;
    uint32_t actuator0_ADCBase = ADC0_BASE + ((1 << 12) * rawADCBase);

    float actuator0_ForceSensorSlope = -1993.222361;
    float actuator0_ForceSensorOffset = 0.973456123;
    tiva.actuator0 = actuatorConstruct(actuatorNumber, actuator0_QEIBase, EncoderMRTypeML_QuadratureEncoder,
                                          actuator0_QEISampleRate, actuator0_QEICountsPerRotation,
                                          actuator0_ADCBase, 0, 'E', 5, Futek_ForceSensor, actuator0_ForceSensorSlope,
                                          4000, -4000,
                                          actuator0_ForceSensorOffset, 20000);

    tivaInit(&tiva);
}

void checkEstop(void)
{
    if (operationEnabled)
    {
        if (EngageVirtualEStop(&tiva))
        {
            tiva.signalToMaster = HALT_SIGNAL_TM;

            // Stop motor
            tiva.actuator0.motorController.dutyCycle = 0;
            tiva.actuator1.motorController.dutyCycle = 0;
            SendPWMSignal(&tiva.actuator0);
            SendPWMSignal(&tiva.actuator1);

            // Send shutdown signal to master
            haltLEDS(&tiva.rgbLed);
            GetAndSendDataToMaster(&tiva.easyCAT);
            tiva.initialized = false;
            tiva.numberOfInitFramesReceived = 0;
        }
        else
        {
            tiva.signalToMaster = NORMAL_OPERATION;
        }
    }
    else
    {
        // Stop motors if not running estop interrupt
        tiva.actuator0.motorController.dutyCycle = 0;
        tiva.actuator1.motorController.dutyCycle = 0;
        SendPWMSignal(&tiva.actuator0);
        SendPWMSignal(&tiva.actuator1);
    }
}

/*
 * EngageVirtualEStop
 * Checks conditions to decide whether to engage Virtual EStop
 * based on Force, Abs Encoder Angle Ranges.
 *
 * @param pandora: a pointer to the pandora struct which contains all of the
 * values to check
 * @return: if the virtual estop should be triggered
 */
bool EngageVirtualEStop(Tiva* tiva)
{
    if(tiva->signalFromMaster == CONTROL_SIGNAL && tiva->settings.softwareEStopEnable)
    {
        // halt codes for joint limits being reached
        if(tiva->joint0.actualRaw < -1 * tiva->joint0.rawBackwardRangeOfMotion)
            tiva->haltCode = ESTOP_JOINT0_BACKWARDRANGE_EXCEEDED;
        else if(tiva->joint0.actualRaw > tiva->joint0.rawForwardRangeOfMotion)
            tiva->haltCode = ESTOP_JOINT0_FORWARDRANGE_EXCEEDED;
        else if(tiva->joint1.actualRaw < -1 * tiva->joint1.rawBackwardRangeOfMotion)
            tiva->haltCode = ESTOP_JOINT1_BACKWARDRANGE_EXCEEDED;
        else if(tiva->joint1.actualRaw > tiva->joint1.rawForwardRangeOfMotion)
            tiva->haltCode = ESTOP_JOINT1_FORWARDRANGE_EXCEEDED;

        // halt codes for force sensor limits being reached
        else if(tiva->actuator0.forceSensor->newtons > tiva->actuator0.forceSensor->upperLimitNewtons)
            tiva->haltCode = ESTOP_FORCE0_UPPERLIMIT_EXCEEDED;
        else if(tiva->actuator0.forceSensor->newtons < tiva->actuator0.forceSensor->lowerLimitNewtons)
            tiva->haltCode = ESTOP_FORCE0_LOWERLIMIT_EXCEEDED;
        else if(tiva->actuator1.forceSensor->newtons > tiva->actuator1.forceSensor->upperLimitNewtons)
            tiva->haltCode = ESTOP_FORCE1_UPPERLIMIT_EXCEEDED;
        else if(tiva->actuator1.forceSensor->newtons < tiva->actuator1.forceSensor->lowerLimitNewtons)
            tiva->haltCode = ESTOP_FORCE1_LOWERLIMIT_EXCEEDED;

        // halt codes for FT sensor force limits being reached
        else if(tiva->ftSensor->forceX > tiva->ftSensor->limits.forceXUpperLimitNewtons)
            tiva->haltCode = ESTOP_FTSENSOR_FORCEX_UPPERLIMIT_EXCEEDED;
        else if(tiva->ftSensor->forceX < tiva->ftSensor->limits.forceXLowerLimitNewtons)
            tiva->haltCode = ESTOP_FTSENSOR_FORCEX_LOWERLIMIT_EXCEEDED;
        else if(tiva->ftSensor->forceY > tiva->ftSensor->limits.forceYUpperLimitNewtons)
            tiva->haltCode = ESTOP_FTSENSOR_FORCEY_UPPERLIMIT_EXCEEDED;
        else if(tiva->ftSensor->forceY < tiva->ftSensor->limits.forceYLowerLimitNewtons)
            tiva->haltCode = ESTOP_FTSENSOR_FORCEY_LOWERLIMIT_EXCEEDED;
        else if(tiva->ftSensor->forceZ > tiva->ftSensor->limits.forceZUpperLimitNewtons)
            tiva->haltCode = ESTOP_FTSENSOR_FORCEZ_UPPERLIMIT_EXCEEDED;
        else if(tiva->ftSensor->forceZ < tiva->ftSensor->limits.forceZLowerLimitNewtons)
            tiva->haltCode = ESTOP_FTSENSOR_FORCEZ_LOWERLIMIT_EXCEEDED;

        // halt codes for FT sensor torque limits being reached
        else if(tiva->ftSensor->torqueX > tiva->ftSensor->limits.torqueXUpperLimitNewtonMeters)
            tiva->haltCode = ESTOP_FTSENSOR_TORQUEX_UPPERLIMIT_EXCEEDED;
        else if(tiva->ftSensor->torqueX < tiva->ftSensor->limits.torqueXLowerLimitNewtonMeters)
            tiva->haltCode = ESTOP_FTSENSOR_TORQUEX_LOWERLIMIT_EXCEEDED;
        else if(tiva->ftSensor->torqueY > tiva->ftSensor->limits.torqueYUpperLimitNewtonMeters)
            tiva->haltCode = ESTOP_FTSENSOR_TORQUEY_UPPERLIMIT_EXCEEDED;
        else if(tiva->ftSensor->torqueY < tiva->ftSensor->limits.torqueYLowerLimitNewtonMeters)
            tiva->haltCode = ESTOP_FTSENSOR_TORQUEY_LOWERLIMIT_EXCEEDED;
        else if(tiva->ftSensor->torqueZ > tiva->ftSensor->limits.torqueZUpperLimitNewtonMeters)
            tiva->haltCode = ESTOP_FTSENSOR_TORQUEZ_UPPERLIMIT_EXCEEDED;
        else if(tiva->ftSensor->torqueZ < tiva->ftSensor->limits.torqueZLowerLimitNewtonMeters)
            tiva->haltCode = ESTOP_FTSENSOR_TORQUEZ_LOWERLIMIT_EXCEEDED;
    }

    return tiva->haltCode != 0;
}

void readSensors()
{
    ReceiveFromEasyCAT(&tiva.easyCAT.etherCATInputFrames);
    tiva.signalFromMaster = tiva.easyCAT.etherCATInputFrames.rawBytes[SIGNAL_INDEX];
    if (tiva.signalFromMaster == CONTROL_SIGNAL)
    {
        unsigned int i;
        for(i = 0; i < tiva.numberOfSensorsEnabled; i++)
            tiva.SensorReadFunctions[i](&tiva);
    }

    // Send TivaToMaster and receive MasterToTiva
    if (runCommunicationLoop || tiva.signalFromMaster != CONTROL_SIGNAL)
    {

        tiva.prevProcessIdFromMaster = tiva.processIdFromMaster;
        tiva.processIdFromMaster = tiva.easyCAT.etherCATInputFrames.rawBytes[PROCESS_ID_INDEX];

        if(tiva.processIdFromMaster != tiva.prevProcessIdFromMaster)
        {
            // Read desired Tiva status, duty cycles, and directions from MasterToTiva
            storeDataFromMaster(&tiva);

            // Act according Tiva status
            operationEnabled = processDataFromMaster(&tiva);
            // Populate TivaToMaster data frame
            loadDataForMaster(&tiva);

            if(tiva.initialized)
                runCommunicationLoop = true;
        }
        else
        {
            if(tiva.signalFromMaster != INITIALIZATION_SIGNAL)
                operationEnabled = processDataFromMaster(&tiva);
            loadDataForMaster(&tiva);
        }
    }
    SendToEasyCAT(&tiva.easyCAT.etherCATOutputFrames);
}
