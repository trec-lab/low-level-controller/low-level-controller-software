/**
 * OrbisEncoder.c
 * Contains all of the low-level code for orbis encoder related functions
 */

#include "OrbisEncoder.h"
#include "../HAL/SSI_TIVA.h"
#include <stdlib.h>

#define ORBIS_ENCODER_RAW_PI    8191

AbsoluteEncoder* OrbisEncoderConstruct(uint32_t SSIBase, uint16_t sampleRate)
{
    static const AbsoluteEncoder_vTable vtable =
    {
         OrbisEncoderEnable,
         OrbisEncoderRead,
         OrbisEncoderFree
    };

    AbsoluteEncoder AbsoluteEncoderBase;
    AbsoluteEncoderBase.vtable = &vtable;

    // store the SSI Base accordingly
    AbsoluteEncoderBase.CommunicationBase = SSIBase;

    // store the sample rate accordingly
    AbsoluteEncoderBase.sampleRate = sampleRate;

    // initialize the other values to raw
    AbsoluteEncoderBase.raw = 0;
    AbsoluteEncoderBase.rawPrev = 0;
    AbsoluteEncoderBase.rawVelF = 0;
    AbsoluteEncoderBase.rawVelPrev = 0;
    AbsoluteEncoderBase.rawVel = 0;

    AbsoluteEncoderBase.rawPi = ORBIS_ENCODER_RAW_PI;

    AbsoluteEncoderBase.enabled = false;
    OrbisEncoder* orbisEncoder = (OrbisEncoder*)malloc(sizeof(OrbisEncoder));
    orbisEncoder->ENCODER_BASE = AbsoluteEncoderBase;

    return &orbisEncoder->ENCODER_BASE;
}

/**
 * OrbisEncoderEnable
 * enables the orbis encoder on the Tiva
 * @param encoder: a pointer to the orbis encoder encoder to enable
 */
void OrbisEncoderEnable(AbsoluteEncoder* encoderBase)
{
    // enable the encoder based on its SSI Base
    // and the brand of the encoder
    if (encoderBase->CommunicationBase == SSI0_BASE)
        SSI0_Orbis_Config();
    else if (encoderBase->CommunicationBase == SSI1_BASE)
        SSI1_Orbis_Config();
    else if (encoderBase->CommunicationBase == SSI2_BASE)
        SSI2_Orbis_Config();
    else if (encoderBase->CommunicationBase == SSI3_BASE)
        SSI3_Orbis_Config();

    // set the encoder to be enabled
    encoderBase->enabled = true;
}

/**
 * OrbisEncoderRead
 * reads the raw position value
 * @param encoder: a pointer to the orbis encoder to read from
 */
void OrbisEncoderRead(AbsoluteEncoder* encoderBase)
{
    SSIDataPut(encoderBase->CommunicationBase, 0);
    while(SSIBusy(encoderBase->CommunicationBase));
    // read and store it in the encoder structure
    SSIDataGet(encoderBase->CommunicationBase, &encoderBase->raw);
}

void OrbisEncoderFree(AbsoluteEncoder* encoderBase)
{
    OrbisEncoder* orbisEncoder = (OrbisEncoder*) encoderBase;
    free(orbisEncoder);
}
