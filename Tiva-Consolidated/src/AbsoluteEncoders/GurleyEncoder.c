/**
 * GurleyEncoder.c
 * Contains all of the low-level code for gurley encoder related functions
 */

#include "GurleyEncoder.h"
#include "../HAL/SSI_TIVA.h"
#include <stdlib.h>

#define GURLEY_ENCODER_RAW_PI   65535

AbsoluteEncoder* GurleyEncoderConstruct(uint32_t SSIBase, uint16_t sampleRate)
{
    static const AbsoluteEncoder_vTable vtable =
    {
         GurleyEncoderEnable,
         GurleyEncoderRead,
         GurleyEncoderFree
    };

    AbsoluteEncoder AbsoluteEncoderBase;
    AbsoluteEncoderBase.vtable = &vtable;

    // store the SSI Base accordingly
    AbsoluteEncoderBase.CommunicationBase = SSIBase;

    // store the sample rate accordingly
    AbsoluteEncoderBase.sampleRate = sampleRate;

    // initialize the other values to raw
    AbsoluteEncoderBase.raw = 0;
    AbsoluteEncoderBase.rawPrev = 0;
    AbsoluteEncoderBase.rawVelF = 0;
    AbsoluteEncoderBase.rawVelPrev = 0;
    AbsoluteEncoderBase.rawVel = 0;

    AbsoluteEncoderBase.rawPi = GURLEY_ENCODER_RAW_PI;

    AbsoluteEncoderBase.enabled = false;
    GurleyEncoder* gurleyEncoder = (GurleyEncoder*)malloc(sizeof(GurleyEncoder));
    gurleyEncoder->ENCODER_BASE = AbsoluteEncoderBase;

    return &gurleyEncoder->ENCODER_BASE;
}

/**
 * GurleyEncoderEnable
 * enables the gurley encoder on the Tiva
 * @param encoder: a pointer to the gurley encoder encoder to enable
 */
void GurleyEncoderEnable(AbsoluteEncoder* encoderBase)
{
    // enable the encoder based on its SSI Base
    // and the brand of the encoder
    if (encoderBase->CommunicationBase == SSI0_BASE)
        SSI0_Gurley_Config();
    else if (encoderBase->CommunicationBase == SSI1_BASE)
        SSI1_Gurley_Config();
    else if (encoderBase->CommunicationBase == SSI2_BASE)
        SSI2_Gurley_Config();
    else if (encoderBase->CommunicationBase == SSI3_BASE)
        SSI3_Gurley_Config();

    // set the encoder to be enabled
    encoderBase->enabled = true;
}

/**
 * GurleyEncoderRead
 * reads the raw position value
 * @param encoder: a pointer to the gurley encoder to read from
 */
void GurleyEncoderRead(AbsoluteEncoder* encoderBase)
{
    SSIDataPut(encoderBase->CommunicationBase, 0);
    while(SSIBusy(encoderBase->CommunicationBase));
    // read and store it in the encoder structure
    SSIDataGet(encoderBase->CommunicationBase, &encoderBase->raw);
}

void GurleyEncoderFree(AbsoluteEncoder* encoderBase)
{
    GurleyEncoder* gurleyEncoder = (GurleyEncoder*) encoderBase;
    free(gurleyEncoder);
}

