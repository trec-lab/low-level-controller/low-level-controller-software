/**
 * GurleyEncoder.c
 * @author: Nick Tremaroli
 * Contains the low-level code for functions related
 * to the Gurley Encoder
 */

#ifndef GURLEY_ENCODER_H
#define GURLEY_ENCODER_H

#include "../AbsoluteEncoder.h"

struct GurleyEncoder
{
    AbsoluteEncoder ENCODER_BASE;

    // add any fields specific to a gurley encoder below
    // ...
};
typedef struct GurleyEncoder GurleyEncoder;

AbsoluteEncoder* GurleyEncoderConstruct(uint32_t SSIBase, uint16_t sampleRate);

void GurleyEncoderEnable(AbsoluteEncoder* encoderBase);

void GurleyEncoderRead(AbsoluteEncoder* encoderBase);

void GurleyEncoderFree(AbsoluteEncoder* encoderBase);

#endif /* GURLEY_ENCODER_H */
