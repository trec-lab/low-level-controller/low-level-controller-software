/**
 * OrbisEncoder.h
 * @author: Nick Tremaroli
 * Contains the low-level code for functions related
 * to the Gurley Encoder
 */
#ifndef ORBIS_ENCODER_H
#define ORBIS_ENCODER_H

#include "../AbsoluteEncoder.h"

struct OrbisEncoder
{
    AbsoluteEncoder ENCODER_BASE;

    // add any fields specific to a orbis encoder below
    // ..
};
typedef struct OrbisEncoder OrbisEncoder;

AbsoluteEncoder* OrbisEncoderConstruct(uint32_t SSIBase, uint16_t sampleRate);

void OrbisEncoderEnable(AbsoluteEncoder* encoderBase);

void OrbisEncoderRead(AbsoluteEncoder* encoderBase);

void OrbisEncoderFree(AbsoluteEncoder* encoderBase);

#endif /* ORBIS_ENCODER_H */
