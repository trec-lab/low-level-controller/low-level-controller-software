/**
 * TivaLowLevel.c
 * @author: Nick Tremaroli
 * Defines all of the low-level features and functions
 * of pandora
 */
#include "TivaLowLevel.h"
#include "HAL/SSI_TIVA.h"

/**
 * tivaConstruct
 *
 * Constructs the tiva structure and sets
 * all of the starting values to 0. Initialization does
 * not happen at this stage
 *
 * @return: a basic pandoraLowLevel structure
 */
Tiva tivaConstruct()
{
    // create the pandora structure
    Tiva tiva;

    // initialize the signals
    tiva.signalToMaster = NORMAL_OPERATION;
    tiva.signalFromMaster = 0;
    tiva.prevSignalFromMaster = 0;

    // initialize the process IDs
    tiva.prevProcessIdFromMaster = 0;
    tiva.processIdFromMaster = 0;

    // set pandora to un-initialized
    tiva.initialized = false;

    tiva.initializedOnce = false;

    // set the number of frames received to 0
    tiva.numberOfInitFramesReceived = 0;

    // no error code to start with
    tiva.haltCode = 0;

    tiva.numberOfSensorsEnabled = 0;

    unsigned int i;
    for(i = 0; i < MAX_SENSORS_ON_TIVA; i++)
    {
        tiva.SensorReadFunctions[i] = 0x00;
        tiva.SensorFreeFunctions[i] = 0x00;
    }

    tiva.PeripheralEnableFunctions[0] = tivaEnablePWM;
    tiva.PeripheralEnableFunctions[1] = tivaEnableRGBLED;
    tiva.numberOfPeripheralsEnabled = 2;
    for(i = tiva.numberOfPeripheralsEnabled; i < MAX_PERIPHERALS_TO_ENABLE_ON_TIVA; i++)
        tiva.PeripheralEnableFunctions[i] = 0x00;

    tiva.currentTivaInitializationFrame = Actuator0InitializationFrame;
    // return the tiva structure
    return tiva;
}

/**
 * tivaInitEtherCAT
 *
 * Initializes the ethercat board on the Tiva. Note that this function
 * is called before the other peripheral based initialization functions.
 * This is because the Tiva must read the initialization input data
 * from the master first to know how to configure the rest of its peripherals
 */
uint8_t tivaInitEtherCAT(Tiva* tiva)
{
    // Configure SSI3 for SPI for use with EtherCAT
    SSI3_EasyCAT_Config_Pandora();

    // Run the EtherCAT_Init function and return the result
    return EasyCAT_Init(&tiva->easyCAT.etherCATOutputFrames);
}

/**
 * ProcessCurrentInitFrame
 *
 * Processes the current initialization frame and applies it to
 * the PandoraLowLevel structure
 * @param pandora: a pointer to the pandora structure from which
 * the initialization frame will be stored to
 */
void StoreCurrentInitFrame(Tiva* tiva)
{
    // get the current initialization frame
    TivaInitializationFrameType currentInitFrame = (TivaInitializationFrameType)tiva->easyCAT.etherCATInputFrames.initSignalHeader.currentInitFrame;

    // actuator 0 is the first initialization frame sent
    if(currentInitFrame == Actuator0InitializationFrame)
    {
        uint8_t actuatorNumber = 0;

        // if the QEI Encoder should be enabled
        uint8_t actuator0_QEIEnable = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_qeiEnable;

        // if the force sensor should be enabled
        uint8_t actuator0_ForceSensorEnable = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_forceSensorEnable;

        // get the raw QEI Base number (0, 1)
        uint8_t rawQEIBase = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_QEIBaseNumber;

        // get the quadrature encoder brand
        uint8_t quadratureEncoderBrandRaw = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_QuadratureEncoderBrand;
        QuadratureEncoderBrand actuator0_QuadratureEncoderBrand = (QuadratureEncoderBrand)quadratureEncoderBrandRaw;

        // convert it to the actual QEI Base
        uint32_t actuator0_QEIBase = QEI0_BASE + ((1 << 12) * rawQEIBase);

        // get the QEI sample rate
        uint16_t actuator0_QEISampleRate = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_QEISampleRate;

        // get the QEI counts per rotation
        uint32_t actuator0_QEICountsPerRotation = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_QEICountsPerRotation;

        // get the raw ADC Base number (0, 1)
        uint8_t rawADCBase = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_ForceSensorADCBaseNumber;

        // get the sequence number of the ADC
        uint8_t actuator0_ADCSequenceNumber = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_ForceSensorADCSequenceNumber;

        // get the GPIO Port Base Letter (GPIOA, GPIOB, etc.)
        char actuator0_ADCPortBaseLetter = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_ForceSensorADCGPIOPortBaseLetter;

        // get the GPIO Pin number of the ADC
        uint8_t actuator0_ADCPinNumber = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_ForceSensorADCPinNumber;

        // get the force sensor brand
        uint8_t forceSensorBrandRaw = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_ForceSensorBrand;
        ForceSensorBrand actuator0_ForceSensorBrand = (ForceSensorBrand)forceSensorBrandRaw;

        // get the force sensor slope
        float actuator0_ForceSensorSlope = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_ForceSensorSlope;

        // get the force sensor offset
        float actuator0_ForceSensorOffset = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_ForceSensorOffset;

        // get the force sensor upper limit in newtons
        float actuator0_ForceSensorUpperLimitNewtons = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_ForceUpperLimitNewtons;

        // get the force sensor lower limit in newtons
        float actuator0_ForceSensorLowerLimitNewtons = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_ForceLowerLimitNewtons;

        // get the motor controller pwm frequency
        uint32_t actuator0_motorControllerPWMFrequency = tiva->easyCAT.etherCATInputFrames.initSignal0Frame.actuator0_MotorControllerPWMFrequency;

        // convert the raw ADC Base Number to the actual ADC Base
        uint32_t actuator0_ADCBase = ADC0_BASE + ((1 << 12) * rawADCBase);

        // construct the pandora actuator on the 0th side
        Actuator actuator0 = actuatorConstruct(actuatorNumber, actuator0_QEIBase, actuator0_QuadratureEncoderBrand,
                                                actuator0_QEISampleRate, actuator0_QEICountsPerRotation,
                                                actuator0_ADCBase, actuator0_ADCSequenceNumber,
                                                actuator0_ADCPortBaseLetter, actuator0_ADCPinNumber, actuator0_ForceSensorBrand,
                                                actuator0_ForceSensorSlope, actuator0_ForceSensorOffset,
                                                actuator0_ForceSensorUpperLimitNewtons, actuator0_ForceSensorLowerLimitNewtons,
                                                actuator0_motorControllerPWMFrequency);

        if(actuator0_QEIEnable)
        {
            tiva->PeripheralEnableFunctions[tiva->numberOfPeripheralsEnabled++] = tivaEnableActuator0QuadratureEncoder;
            tiva->SensorReadFunctions[tiva->numberOfSensorsEnabled] = tivaReadActuator0QuadratureEncoder;
            tiva->SensorFreeFunctions[tiva->numberOfSensorsEnabled++] = tivaFreeActuator0QuadratureEncoder;
        }
        if(actuator0_ForceSensorEnable)
        {
            tiva->PeripheralEnableFunctions[tiva->numberOfPeripheralsEnabled++] = tivaEnableActuator0ForceSensor;
            tiva->SensorReadFunctions[tiva->numberOfSensorsEnabled] = tivaReadActuator0ForceSensor;
            tiva->SensorFreeFunctions[tiva->numberOfSensorsEnabled++] = tivaFreeActuator0ForceSensor;
        }

        // store it to the pandora structure
        tiva->actuator0 = actuator0;

    }

    // actuator 1 is the second initialization frame send
    else if(currentInitFrame == Actuator1InitializationFrame)
    {
        uint8_t actuatorNumber = 1;

        // if the QEI Encoder should be enabled
        uint8_t actuator1_QEIEnable = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_qeiEnable;

        // if the force sensor should be enabled
        uint8_t actuator1_ForceSensorEnable = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_forceSensorEnable;

        // get the raw QEI Base number (0, 1)
        uint8_t rawQEIBase = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_QEIBaseNumber;

        // convert it to the actual QEI Base
        uint32_t actuator1_QEIBase = QEI0_BASE + ((1 << 12) * rawQEIBase);

        // get the quadrature encoder brand
        uint8_t quadratureEncoderBrandRaw = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_QuadratureEncoderBrand;
        QuadratureEncoderBrand actuator1_QuadratureEncoderBrand = (QuadratureEncoderBrand)quadratureEncoderBrandRaw;

        // get the QEI sample rate
        uint16_t actuator1_QEISampleRate = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_QEISampleRate;

        // get the QEI counts per rotation
        uint32_t actuator1_QEICountsPerRotation = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_QEICountsPerRotation;

        // get the raw ADC Base number (0, 1)
        uint8_t rawADCBase = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_ForceSensorADCBaseNumber;


        // get the sequence number of the ADC
        uint8_t actuator1_ADCSequenceNumber = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_ForceSensorADCSequenceNumber;

        // get the GPIO Port Base Letter (GPIOA, GPIOB, etc.)
        char actuator1_ADCPortBaseLetter = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_ForceSensorADCGPIOPortBaseLetter;

        // get the GPIO Pin number of the ADC
        uint8_t actuator1_ADCPinNumber = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_ForceSensorADCPinNumber;

        // get the force sensor brand
        uint8_t forceSensorBrandRaw = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_ForceSensorBrand;
        ForceSensorBrand actuator1_ForceSensorBrand = (ForceSensorBrand)forceSensorBrandRaw;

        // get the force sensor slope
        float actuator1_ForceSensorSlope = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_ForceSensorSlope;

        // get the force sensor offset
        float actuator1_ForceSensorOffset = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_ForceSensorOffset;

        // get the force sensor upper limit in newtons
        float actuator1_ForceSensorUpperLimitNewtons = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_ForceUpperLimitNewtons;

        // get the force sensor lower limit in newtons
        float actuator1_ForceSensorLowerLimitNewtons = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_ForceLowerLimitNewtons;

        // get the motor controller PWM frequency
        uint32_t actuator1_motorControllerPWMFrequency = tiva->easyCAT.etherCATInputFrames.initSignal1Frame.actuator1_MotorControllerPWMFrequency;

        // convert it to the actual ADC Base
        uint32_t actuator1_ADCBase = ADC0_BASE + ((1 << 12) * rawADCBase);

        // construct the pandora actuator on the 0th side
        Actuator actuator1 = actuatorConstruct(actuatorNumber, actuator1_QEIBase, actuator1_QuadratureEncoderBrand,
                                                 actuator1_QEISampleRate, actuator1_QEICountsPerRotation,
                                                 actuator1_ADCBase, actuator1_ADCSequenceNumber,
                                                 actuator1_ADCPortBaseLetter, actuator1_ADCPinNumber, actuator1_ForceSensorBrand,
                                                 actuator1_ForceSensorSlope, actuator1_ForceSensorOffset,
                                                 actuator1_ForceSensorUpperLimitNewtons, actuator1_ForceSensorLowerLimitNewtons,
                                                 actuator1_motorControllerPWMFrequency);

        if(actuator1_QEIEnable)
        {
            tiva->PeripheralEnableFunctions[tiva->numberOfPeripheralsEnabled++] = tivaEnableActuator1QuadratureEncoder;
            tiva->SensorReadFunctions[tiva->numberOfSensorsEnabled] = tivaReadActuator1QuadratureEncoder;
            tiva->SensorFreeFunctions[tiva->numberOfSensorsEnabled++] = tivaFreeActuator1QuadratureEncoder;
        }
        if(actuator1_ForceSensorEnable)
        {
            tiva->PeripheralEnableFunctions[tiva->numberOfPeripheralsEnabled++] = tivaEnableActuator1ForceSensor;
            tiva->SensorReadFunctions[tiva->numberOfSensorsEnabled] = tivaReadActuator1ForceSensor;
            tiva->SensorFreeFunctions[tiva->numberOfSensorsEnabled++] = tivaFreeActuator1ForceSensor;
        }
        // store it to the pandora structure
        tiva->actuator1 = actuator1;
    }

    // joint 0 is the third initialization frame send
    else if(currentInitFrame == Joint0InitializationFrame)
    {
        // if joint0 should be enabled
        uint8_t joint0_enable = tiva->easyCAT.etherCATInputFrames.initSignal2Frame.joint0_enable;

        // get the raw SSI Base number (0, 1, etc.)
        uint8_t rawSSIBaseNumber = tiva->easyCAT.etherCATInputFrames.initSignal2Frame.joint0_AbsoluteEncoderSSICommunicationBaseNumber;

        // convert it to the actual SSI Base
        uint32_t joint0_SSIBaseNumber = SSI0_BASE + ((1 << 12) * rawSSIBaseNumber);

        // get the SSI Brand
        uint8_t absoluteEncoderBrandRaw = tiva->easyCAT.etherCATInputFrames.initSignal2Frame.joint0_AbsoluteEncoderBrandRaw;
        AbsoluteEncoderBrand joint0_AbsoluteEncoderBrand = (AbsoluteEncoderBrand)absoluteEncoderBrandRaw;

        // get the SSI sample rate
        uint16_t joint0_AbsoluteEncoderSampleRate = tiva->easyCAT.etherCATInputFrames.initSignal2Frame.joint0_AbsoluteEncoderSampleRate;

        // get the joint reverse factor
        int8_t joint0_jointReverseFactor = tiva->easyCAT.etherCATInputFrames.initSignal2Frame.joint0_ReverseFactor;

        // get the actual zero
        float joint0_zeroPositionDegrees = tiva->easyCAT.etherCATInputFrames.initSignal2Frame.joint0_RawZeroPosition;

        // get the actual forward range of motion
        float joint0_forwardRangeOfMotionDegrees = tiva->easyCAT.etherCATInputFrames.initSignal2Frame.joint0_RawForwardRangeOfMotion;

        // get the actual backward range of motion
        float joint0_backwardRangeOfMotionDegrees = tiva->easyCAT.etherCATInputFrames.initSignal2Frame.joint0_RawBackwardRangeOfMotion;

        // construct the pandora joint on the 0th side
        Joint joint0 = jointConstruct(joint0_SSIBaseNumber, joint0_AbsoluteEncoderBrand, joint0_AbsoluteEncoderSampleRate,
                                      joint0_jointReverseFactor, joint0_zeroPositionDegrees,
                                      joint0_forwardRangeOfMotionDegrees, joint0_backwardRangeOfMotionDegrees);

        if(joint0_enable)
        {
            tiva->PeripheralEnableFunctions[tiva->numberOfPeripheralsEnabled++] = tivaEnableJoint0AbsoluteEncoder;
            tiva->SensorReadFunctions[tiva->numberOfSensorsEnabled] = tivaReadJoint0Angles;
            tiva->SensorFreeFunctions[tiva->numberOfSensorsEnabled++] = tivaFreeJoint0AbsoluteEncoder;
        }
        // store it to the pandora structure
        tiva->joint0 = joint0;
    }

    // joint 1 is the fourth initialization frame sent
    else if(currentInitFrame == Joint1InitializationFrame)
    {
        // if joint1 should be enabled
        uint8_t joint1_enable = tiva->easyCAT.etherCATInputFrames.initSignal3Frame.joint1_enable;

        // get the raw SSI Base number (0, 1, etc.)
        uint8_t rawSSIBaseNumber = tiva->easyCAT.etherCATInputFrames.initSignal3Frame.joint1_AbsoluteEncoderSSICommunicationBaseNumber;

        // convert it to the actual SSI Base
        uint32_t joint1_SSIBaseNumber = SSI0_BASE + ((1 << 12) * rawSSIBaseNumber);

        // get the SSI Brand
        uint8_t absoluteEncoderBrandRaw = tiva->easyCAT.etherCATInputFrames.initSignal3Frame.joint1_AbsoluteEncoderBrandRaw;
        AbsoluteEncoderBrand joint1_SSIEncoderBrand = (AbsoluteEncoderBrand)absoluteEncoderBrandRaw;

        // get the SSI sample rate
        uint16_t joint1_AbsoluteEncoderSampleRate = tiva->easyCAT.etherCATInputFrames.initSignal3Frame.joint1_AbsoluteEncoderSampleRate;

        // get the joint reverse factor
        int8_t joint1_jointReverseFactor = tiva->easyCAT.etherCATInputFrames.initSignal3Frame.joint1_ReverseFactor;

        // get the actual zero
        float joint1_ZeroPositionDegrees = tiva->easyCAT.etherCATInputFrames.initSignal3Frame.joint1_RawZeroPosition;

        // get the actual forward range of motion
        float joint1_forwardRangeOfMotionDegrees = tiva->easyCAT.etherCATInputFrames.initSignal3Frame.joint1_RawForwardRangeOfMotion;

        // get the actual backward range of motion
        float joint1_backwardRangeOfMotionDegrees = tiva->easyCAT.etherCATInputFrames.initSignal3Frame.joint1_RamBackwardRangeOfMotion;

        // construct the pandora joint on the 1th side
        Joint joint1 = jointConstruct(joint1_SSIBaseNumber, joint1_SSIEncoderBrand, joint1_AbsoluteEncoderSampleRate,
                                      joint1_jointReverseFactor, joint1_ZeroPositionDegrees,
                                      joint1_forwardRangeOfMotionDegrees, joint1_backwardRangeOfMotionDegrees);

        if(joint1_enable)
        {
            tiva->PeripheralEnableFunctions[tiva->numberOfPeripheralsEnabled++] = tivaEnableJoint1AbsoluteEncoder;
            tiva->SensorReadFunctions[tiva->numberOfSensorsEnabled] = tivaReadJoint1Angles;
            tiva->SensorFreeFunctions[tiva->numberOfSensorsEnabled++] = tivaFreeJoint1AbsoluteEncoder;
        }
        // store it to the pandora structure
        tiva->joint1 = joint1;
    }

    // the LED configuration is sent in the fifth initialization frame
    else if(currentInitFrame == RGBLEDInitializationFrame)
    {
        char rawRedLEDPinBaseLetter = tiva->easyCAT.etherCATInputFrames.initSignal4Frame.redGPIOPinBaseLetter;
        uint8_t rawRedLEDPinNumber = tiva->easyCAT.etherCATInputFrames.initSignal4Frame.redGPIOPinNumber;
        uint32_t redLEDPinBase;
        if(rawRedLEDPinBaseLetter <= 0x44) // D in hex
            redLEDPinBase = GPIO_PORTA_BASE + ((1 << 12) * (rawRedLEDPinBaseLetter - 0x41));
        else
            redLEDPinBase = GPIO_PORTE_BASE + ((1 << 12) * rawRedLEDPinBaseLetter - 0x45);
        uint32_t redLEDPin = (GPIO_PIN_0 << rawRedLEDPinNumber);
        uint32_t redSysCtlPeripheralBase = SYSCTL_PERIPH_GPIOA + (rawRedLEDPinBaseLetter - 0x41);

        char rawGreenLEDPinBaseLetter = tiva->easyCAT.etherCATInputFrames.initSignal4Frame.greenGPIOPinBaseLetter;
        uint8_t rawGreenLEDPinNumber = tiva->easyCAT.etherCATInputFrames.initSignal4Frame.greenGPIOPinNumber;
        uint32_t greenLEDPinBase;
        if(rawGreenLEDPinBaseLetter <= 0x44) // D in hex
            greenLEDPinBase = GPIO_PORTA_BASE + ((1 << 12) * (rawGreenLEDPinBaseLetter - 0x41));
        else
            greenLEDPinBase = GPIO_PORTE_BASE + ((1 << 12) * (rawGreenLEDPinBaseLetter - 0x45));

        uint32_t greenLEDPin = (GPIO_PIN_0 << rawGreenLEDPinNumber);
        uint32_t greenSysCtlPeripheralBase = SYSCTL_PERIPH_GPIOA + (rawGreenLEDPinBaseLetter - 0x41);

        char rawBlueLEDPinBaseLetter = tiva->easyCAT.etherCATInputFrames.initSignal4Frame.blueGPIOPinBaseLetter;
        uint8_t rawBlueLEDPinNumber = tiva->easyCAT.etherCATInputFrames.initSignal4Frame.blueGPIOPinNumber;
        uint32_t blueLEDPinBase;
        if(rawBlueLEDPinBaseLetter <= 0x44) // D in hex
            blueLEDPinBase = GPIO_PORTA_BASE + ((1 << 12) * (rawBlueLEDPinBaseLetter - 0x41));
        else
            blueLEDPinBase = GPIO_PORTE_BASE + ((1 << 12) * (rawBlueLEDPinBaseLetter - 0x45));

        uint32_t blueLEDPin = (GPIO_PIN_0 << rawBlueLEDPinNumber);
        uint32_t blueSysCtlPeripheralBase = SYSCTL_PERIPH_GPIOA + (rawBlueLEDPinBaseLetter - 0x41);

        RGBLED rgbLed = rgbLEDConstruct(redSysCtlPeripheralBase, redLEDPinBase, redLEDPin,
                                        greenSysCtlPeripheralBase, greenLEDPinBase, greenLEDPin,
                                        blueSysCtlPeripheralBase, blueLEDPinBase, blueLEDPin);
        tiva->rgbLed = rgbLed;
    }

    // the IMU enable feature is sent in the sixth initialization frame
    else if(currentInitFrame == IMUInitializationFrame)
    {
        // get the imu enable feature
        uint8_t imuEnable = tiva->easyCAT.etherCATInputFrames.initSignal5Frame.imuEnable;

        uint8_t rawI2CBaseNumber = tiva->easyCAT.etherCATInputFrames.initSignal5Frame.imu_I2CBaseNumber;

        uint32_t I2CBase =  I2C0_BASE + ((1 << 12) * rawI2CBaseNumber);

        uint8_t imuBrandRaw = tiva->easyCAT.etherCATInputFrames.initSignal5Frame.imu_IMUBrand;
        IMUBrand imuBrand = (IMUBrand) imuBrandRaw;

        if(imuBrand == MPU9250_IMU)
            tiva->imu = MPU9250Construct(I2CBase);

        if(imuEnable)
        {
            tiva->PeripheralEnableFunctions[tiva->numberOfPeripheralsEnabled++] = tivaEnableIMU;
            tiva->SensorReadFunctions[tiva->numberOfSensorsEnabled] = tivaReadIMUData;
            tiva->SensorFreeFunctions[tiva->numberOfSensorsEnabled++] = tivaFreeIMU;
        }
    }

    // the FT sensor initialization frame is the seventh initialization frame
    else if(currentInitFrame == FTSensorInitializationFrame)
    {
        // if the FT sensor should be enabled
        uint8_t ftSensorEnable = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.ftSensorEnable;

        uint32_t canRate = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.ftSensor_canRate;

        uint8_t rawCANBaseNumber = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.ftSensor_canBaseNumber;

        uint8_t ftSensorBrandRaw = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.ftSensor_FTSensorBrandRaw;
        FTSensorBrand ftSensorBrand = (FTSensorBrand) ftSensorBrandRaw;

        uint32_t canBase = CAN0_BASE + ((1 << 12) * rawCANBaseNumber);

        char rawCANBaseLetter = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.ftSensor_canBaseLetter;

        // the sensor limits
        FTSensorLimits ftSensorLimits;

        ftSensorLimits.forceXUpperLimitNewtons = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.forceXUpperLimitNewtons;
        ftSensorLimits.forceXLowerLimitNewtons = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.forceXLowerLimitNewtons;
        ftSensorLimits.forceYUpperLimitNewtons = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.forceYUpperLimitNewtons;
        ftSensorLimits.forceYLowerLimitNewtons = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.forceYLowerLimitNewtons;
        ftSensorLimits.forceZUpperLimitNewtons = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.forceZUpperLimitNewtons;
        ftSensorLimits.forceZLowerLimitNewtons = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.forceZLowerLimitNewtons;

        ftSensorLimits.torqueXUpperLimitNewtonMeters = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.torqueXUpperLimitNewtonMeters;
        ftSensorLimits.torqueXLowerLimitNewtonMeters = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.torqueXLowerLimitNewtonMeters;
        ftSensorLimits.torqueYUpperLimitNewtonMeters = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.torqueYUpperLimitNewtonMeters;
        ftSensorLimits.torqueYLowerLimitNewtonMeters = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.torqueYLowerLimitNewtonMeters;
        ftSensorLimits.torqueZUpperLimitNewtonMeters = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.torqueZUpperLimitNewtonMeters;
        ftSensorLimits.torqueZLowerLimitNewtonMeters = tiva->easyCAT.etherCATInputFrames.initSignal6Frame.torqueZLowerLimitNewtonMeters;

        FTSensor* ftSensor = 0;
        if(ftSensorBrand == ATIForceTorqueTransducer_FTSensor)
            ftSensor = ATIForceTorqueTransducerConstruct(canRate, canBase, rawCANBaseLetter, ftSensorLimits);
        if(ftSensorEnable)
        {
            tiva->PeripheralEnableFunctions[tiva->numberOfPeripheralsEnabled++] = tivaEnableFTSensor;
            tiva->SensorReadFunctions[tiva->numberOfSensorsEnabled] = tivaReadFTSensorData;
            tiva->SensorFreeFunctions[tiva->numberOfSensorsEnabled++] = tivaFreeFTSensor;
        }

        tiva->ftSensor = ftSensor;
    }
    // the settings and estop enable feature is sent in the
    // eight initialization frame
    else if(currentInitFrame == 7)
    {
        // get the estop enable option
        uint8_t softwareEStopEnable = tiva->easyCAT.etherCATInputFrames.initSignal7Frame.softwareEStopEnable;

        // create a pandora settings structure and store the value to it
        TivaSettings settings;
        settings.softwareEStopEnable = softwareEStopEnable;

        // store the settings in the pandora structure
        tiva->settings = settings;
    }
}

/**
 * tivaInit
 *
 * The initialization function which initializes
 * all of the necessary Tiva peripherals
 *
 * @param pandora: a pointer to the pandora structure
 */
void tivaInit(Tiva* tiva)
{
    unsigned int i;
    for(i = 0; i < tiva->numberOfPeripheralsEnabled; i++)
        tiva->PeripheralEnableFunctions[i](tiva);

    #ifndef RTOS
        // do not reconfigure the timers
        // on re-initialization
        if(!tiva->initializedOnce)
        {
            timer1A_Config();
            timer2A_Config();
            timer3A_Config();
        }
    #endif
}

/*-----------------------Tiva peripheral enabled functions-----------------------*/
void tivaEnablePWM(Tiva* tiva)
{
    PWMConfig();
}

void tivaEnableRGBLED(Tiva* tiva)
{
    enableRGBLED(&tiva->rgbLed);
}

void tivaEnableActuator0QuadratureEncoder(Tiva* tiva)
{
    QuadratureEncoderEnable(tiva->actuator0.motorEncoder);
}

void tivaEnableActuator0ForceSensor(Tiva* tiva)
{
    ForceSensorEnable(tiva->actuator0.forceSensor);
}

void tivaEnableActuator1QuadratureEncoder(Tiva* tiva)
{
    QuadratureEncoderEnable(tiva->actuator1.motorEncoder);
}

void tivaEnableActuator1ForceSensor(Tiva* tiva)
{
    ForceSensorEnable(tiva->actuator1.forceSensor);
}

void tivaEnableJoint0AbsoluteEncoder(Tiva* tiva)
{
    AbsoluteEncoderEnable(tiva->joint0.encoder);
}

void tivaEnableJoint1AbsoluteEncoder(Tiva* tiva)
{
    AbsoluteEncoderEnable(tiva->joint1.encoder);
}

void tivaEnableIMU(Tiva* tiva)
{
    IMUEnable(tiva->imu);
}

void tivaEnableFTSensor(Tiva* tiva)
{
    FTSensorEnable(tiva->ftSensor);
}

/*-----------------------Tiva sensor read functions-----------------------*/
void tivaReadActuator0QuadratureEncoder(Tiva* tiva)
{
    QuadratureEncoderReadPosition(tiva->actuator0.motorEncoder);
    QuadratureEncoderReadVelocity(tiva->actuator0.motorEncoder);
}

void tivaReadActuator0ForceSensor(Tiva* tiva)
{
    ForceSensorReadForce(tiva->actuator0.forceSensor);
}

void tivaReadActuator1QuadratureEncoder(Tiva* tiva)
{
    QuadratureEncoderReadPosition(tiva->actuator1.motorEncoder);
    QuadratureEncoderReadVelocity(tiva->actuator1.motorEncoder);
}

void tivaReadActuator1ForceSensor(Tiva* tiva)
{
    ForceSensorReadForce(tiva->actuator1.forceSensor);
}

void tivaReadJoint0Angles(Tiva* tiva)
{
    updateJointAngles(&tiva->joint0);
}

void tivaReadJoint1Angles(Tiva* tiva)
{
    updateJointAngles(&tiva->joint1);
}

void tivaReadIMUData(Tiva* tiva)
{
    IMURead(tiva->imu);
}

void tivaReadFTSensorData(Tiva* tiva)
{
    FTSensorRead(tiva->ftSensor);
}

/*-----------------------Tiva sensor free functions-----------------------*/
void tivaFreeActuator0QuadratureEncoder(Tiva* tiva)
{
    QuadratureEncoderFree(tiva->actuator0.motorEncoder);
}

void tivaFreeActuator0ForceSensor(Tiva* tiva)
{
    ForceSensorFree(tiva->actuator0.forceSensor);
}

void tivaFreeActuator1QuadratureEncoder(Tiva* tiva)
{
    QuadratureEncoderFree(tiva->actuator1.motorEncoder);
}

void tivaFreeActuator1ForceSensor(Tiva* tiva)
{
    ForceSensorFree(tiva->actuator1.forceSensor);
}

void tivaFreeJoint0AbsoluteEncoder(Tiva* tiva)
{
    AbsoluteEncoderFree(tiva->joint0.encoder);
}

void tivaFreeJoint1AbsoluteEncoder(Tiva* tiva)
{
    AbsoluteEncoderFree(tiva->joint1.encoder);
}

void tivaFreeIMU(Tiva* tiva)
{
    IMUFree(tiva->imu);
}

void tivaFreeFTSensor(Tiva* tiva)
{
    FTSensorFree(tiva->ftSensor);
}

/**
 * storeDataFromMaster
 *
 * Deserializes the data received from the master
 * and stores the data accordingly.
 *
 * @param pandora: a pointer to the pandora structure,
 * all of the data from the master gets stored in this
 * structure
 */
void storeDataFromMaster(Tiva* tiva)
{
    // get the signal from the master computer
    tiva->signalFromMaster = tiva->easyCAT.etherCATInputFrames.rawBytes[SIGNAL_INDEX];

    // if a control signal is received
    if (tiva->signalFromMaster == CONTROL_SIGNAL)
    {
        // Store joint 0 direction
        tiva->actuator0.motorController.direction = tiva->easyCAT.etherCATInputFrames.controlSignalFrame.actuator0Direction;

        // Store joint 0 duty cycle
        tiva->actuator0.motorController.dutyCycle = tiva->easyCAT.etherCATInputFrames.controlSignalFrame.actuator0DutyCycle;

        // Store joint 1 direction
        tiva->actuator1.motorController.direction = tiva->easyCAT.etherCATInputFrames.controlSignalFrame.actuator1Direction;

        // Store joint 1 duty cycle
        tiva->actuator1.motorController.dutyCycle = tiva->easyCAT.etherCATInputFrames.controlSignalFrame.actuator1DutyCycle;
    }

    // if an initialization signal is received
    else if (tiva->signalFromMaster == INITIALIZATION_SIGNAL)
        // increment how many initialization frames have been received
        tiva->numberOfInitFramesReceived = tiva->easyCAT.etherCATInputFrames.initSignalHeader.currentInitFrame + 1;
}

/**
 * processDataFromMaster
 *
 * Processes the data received form the master
 * Acts on received master data according to desired TIVA mode
 *
 * @param pandora: a pointer to the pandora structure
 * @return: 1 if operation of the robot should be enabled, otherwise return 0.
 */
bool processDataFromMaster(Tiva* tiva)
{
    /***********FOR INITIALIZATION***********/
    // if an initialization signal is received
    if(tiva->signalFromMaster == INITIALIZATION_SIGNAL)
    {
        // if pandora is already initialized, setup for
        // re-initialization
        if ((tiva->initialized && tiva->initializedOnce) || (tiva->initializedOnce && tiva->haltCode != 0))
        {
            unsigned int i;
            // free all of the sensors
            for(i = 0; i < tiva->numberOfSensorsEnabled; i++)
                tiva->SensorFreeFunctions[i](tiva);
            tiva->initialized = false;
            tiva->haltCode = 0;
            tiva->numberOfInitFramesReceived = 0;
            tiva->numberOfSensorsEnabled = 0;

            for(i = 0; i < MAX_SENSORS_ON_TIVA; i++)
            {
                // clear the sensor free functions
                tiva->SensorFreeFunctions[i] = 0x00;

                // clear the sensor read functions
                tiva->SensorReadFunctions[i] = 0x00;
            }
            tiva->PeripheralEnableFunctions[0] = tivaEnablePWM;
            tiva->PeripheralEnableFunctions[1] = tivaEnableRGBLED;
            tiva->numberOfPeripheralsEnabled = 2;
            for(i = tiva->numberOfPeripheralsEnabled; i < MAX_PERIPHERALS_TO_ENABLE_ON_TIVA; i++)
                tiva->PeripheralEnableFunctions[i] = 0x00;

        }
        // store the current initialization frame which was sent
        StoreCurrentInitFrame(tiva);

        // if all of the initialization frames were sent
        if(tiva->numberOfInitFramesReceived == NUMBER_OF_INITIALIZATION_FRAMES)
        {
            // set pandora to be initialized
            tiva->haltCode = 0;
            tiva->signalToMaster = NORMAL_OPERATION;
            tiva->initialized = true;
            // if the tiva has already been initialized before this re-initialization call
            // then the tiva needs to be re-initialized
            if(tiva->initializedOnce)
                tivaInit(tiva);
            return true;
        }

        // update the previous signal from master
        tiva->prevSignalFromMaster = tiva->signalFromMaster;
        return false;
    }

    // reset the number of initialization frames received if
    // an initialization signal is not being sent and pandora is
    // already initialized
    else if(tiva->signalFromMaster != INITIALIZATION_SIGNAL && tiva->initialized)
        tiva->numberOfInitFramesReceived = 0;

    // if pandora is not initialized
    if(!tiva->initialized)
    {
        // update the previous signal from master
        tiva->prevSignalFromMaster = tiva->signalFromMaster;
        return false;
    }



    /***********FOR ALL OTHER SIGNALS***********/
    bool operationEnabled = false;

    // if the master computer is sending a control signal
    if (tiva->signalFromMaster == CONTROL_SIGNAL && tiva->signalToMaster != HALT_SIGNAL_TM)
    {
        // the LEDs will be solid Blue
        controlLEDS(&tiva->rgbLed);

        // Send motor PWMs and directions to motor controllers
        SendPWMSignal(&tiva->actuator0);
        SendPWMSignal(&tiva->actuator1);

        // Run estop interrupt
        operationEnabled = true;
    }
    else
    {
        tiva->actuator0.motorController.dutyCycle = 0;
        tiva->actuator1.motorController.dutyCycle = 0;
        SendPWMSignal(&tiva->actuator0);
        SendPWMSignal(&tiva->actuator1);
    }

    // if the master computer is not connected
    if (tiva->signalFromMaster == NOT_CONNECTED)
        // the LEDs will flash Red
        notConnectedLEDS(&tiva->rgbLed);

    // if the master computer or the Tiva sends a halt signal
    else if (tiva->signalFromMaster == HALT_SIGNAL || tiva->signalToMaster == HALT_SIGNAL_TM)
        // the LEDs will be solid Red
        haltLEDS(&tiva->rgbLed);

    // if the master computer sends out an Idle Signal
    else if (tiva->signalFromMaster == IDLE_SIGNAL)
        // the LEDs will be solid purple
        idleLEDS(&tiva->rgbLed);

    // update the previous signal from master
    tiva->prevSignalFromMaster = tiva->signalFromMaster;

    return operationEnabled;
}

/**
 * loadDataForMaster
 *
 * serializes the data to send back to the master computer
 * over ethercat and puts all the data in its corresponding
 * index within the frame to send back.
 *
 * @param pandora: a pointer to the pandora structure,
 * the data to send is serialized from this structure
 */
void loadDataForMaster(Tiva* tiva)
{
    // echo the signal the master was sending out
    if(tiva->signalToMaster == NORMAL_OPERATION || tiva->signalFromMaster == INITIALIZATION_SIGNAL)
        tiva->signalToMaster = tiva->signalFromMaster;
    // load the signal frame in the etherCAT output frame
    tiva->easyCAT.etherCATOutputFrames.rawBytes[SIGNAL_INDEX] = (uint8_t)tiva->signalToMaster;

    // load the master process ID frame in the etherCAT output frame
    tiva->easyCAT.etherCATOutputFrames.rawBytes[PROCESS_ID_INDEX] = tiva->processIdFromMaster;

    // if the signal from master is a control signal
    if(tiva->signalFromMaster == CONTROL_SIGNAL || tiva->signalFromMaster == HALT_SIGNAL)
    {
        MPU9250* tivaMPU9250 = (MPU9250*) tiva->imu;

        // Package force sensor 0 Newton value
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.actuator0ForceInNewtons = tiva->actuator0.forceSensor->newtons;

        // Package actuator 0 motor encoder raw position
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.actuator0MotorEncoderRawPosition = tiva->actuator0.motorEncoder->raw;

        // Package actuator 0 motor encoder direction
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.actuator0MotorEncoderDirection = tiva->actuator0.motorEncoder->direction;

        // Package actuator 0 motor encoder velocity
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.actuator0MotorEncoderRawVelocity = tiva->actuator0.motorEncoder->rawVel;

        // Package force sensor 1 Newton value
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.actuator1ForceInNewtons = tiva->actuator1.forceSensor->newtons;

        // Package actuator 1 motor encoder raw position
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.actuator1MotorEncoderRawPosition = tiva->actuator1.motorEncoder->raw;

        // Package actuator 1 motor encoder direction
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.actuator1MotorEncoderDirection = tiva->actuator1.motorEncoder->direction;

        // Package actuator 1 motor encoder velocity
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.actuator1MotorEncoderRawVelocity = tiva->actuator1.motorEncoder->rawVel;

        // Package encoder 0 radian value
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.joint0angleRadians = tiva->joint0.angleRads;

        // Package encoder 1 radian value
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.joint1angleRadians = tiva->joint1.angleRads;

        // Package IMU Acceleration X
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.Ax = tiva->imu->accelerationData.Ax;

        // Package IMU Acceleration Y
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.Ay = tiva->imu->accelerationData.Ay;

        // Package IMU Acceleration Z
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.Az = tiva->imu->accelerationData.Az;

        // Package IMU Gyro X
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.Gx = tiva->imu->gyroData.Gx;

        // Package IMU Gyro Y
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.Gy = tiva->imu->gyroData.Gy;

        // Package IMU Gyro Z
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.Gz = tiva->imu->gyroData.Gz;

        // Package IMU Magnetometer X
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.Mx = tivaMPU9250->magnetometerData.Mx;

        // Package IMU Magnetometer Y
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.My = tivaMPU9250->magnetometerData.My;

        // Package IMU Magnetometer Z
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.Mz = tivaMPU9250->magnetometerData.Mz;

        // Package FT Sensor Force X
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.ftForceX = tiva->ftSensor->forceX;

        // Package FT Sensor Force Y
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.ftForceY = tiva->ftSensor->forceY;

        // Package FT Sensor Force Z
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.ftForceZ = tiva->ftSensor->forceZ;

        // Package FT Sensor Torque X
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.ftTorqueX = tiva->ftSensor->torqueX;

        // Package FT Sensor Torque Y
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.ftTorqueY = tiva->ftSensor->torqueY;

        // Package FT Sensor Torque Z
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.ftTorqueZ = tiva->ftSensor->torqueZ;

        // A halt code to send to the master if there is one
        tiva->easyCAT.etherCATOutputFrames.controlSignalFrame.haltCodeToMaster = tiva->haltCode;
    }
    // if the signal from master is an initialization signal
    else if(tiva->signalFromMaster == INITIALIZATION_SIGNAL)
    {
        // Package the number of initialization frames received
        tiva->easyCAT.etherCATOutputFrames.initSignalFrame.numInitializationFramesReceived = tiva->numberOfInitFramesReceived;

        // Package the total number of initialization frames to receive
        tiva->easyCAT.etherCATOutputFrames.initSignalFrame.totalNumberOfInitializationFrames = NUMBER_OF_INITIALIZATION_FRAMES;
    }
}

