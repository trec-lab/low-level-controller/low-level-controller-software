/**
 * ForceSensor.c
 * @author: Nick Tremaroli
 * Contains all of the low-level code for force sensor related functions
 */
#include "ForceSensor.h"

void ForceSensorEnable(ForceSensor* forceSensor)
{
    forceSensor->vtable->ForceSensorEnable(forceSensor);
}

void ForceSensorReadForce(ForceSensor* forceSensor)
{
    forceSensor->vtable->ForceSensorReadForce(forceSensor);
}

void ForceSensorFree(ForceSensor* forceSensor)
{
    forceSensor->vtable->ForceSensorReadFree(forceSensor);
}
